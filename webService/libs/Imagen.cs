﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Configuration;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Web;

namespace webService.libs
{
    public class Imagen
    {
        public string ImagenFile { get; set; }
        public string idArticulo { get; set; }
        public string Directiorio { get; set; }
        public int NewAncho { get; set; }
        public int NewAlto { get; set; }

        public string ScaleImage( byte[] img64 , int size, string folderName )
        {
            
            MemoryStream ms = new MemoryStream();

            ms.Write(img64,0, img64.Length);

            Image img = Image.FromStream(ms, true);
          
            if ( size != 0 )
            {
                if ( img.Width > img.Height)
                {
                    decimal x = (decimal)size / (decimal)img.Height;

                    NewAncho = (int)(x * (decimal)img.Width);
                    NewAlto = size;


                }
                else
                {
                    decimal c = (decimal)size / (decimal)img.Width;

                    NewAncho = size;
                    NewAlto = (int)(c * (decimal)img.Height);

                }

            } else
            {
                NewAncho = img.Width;
                NewAlto = img.Height;
            }

            var newImage = new Bitmap( NewAncho, NewAlto );

            using (var g = Graphics.FromImage(newImage))
            {
                g.DrawImage(img, 0, 0, NewAncho, NewAlto );
            }

            // GUARDANDO IMAGEN CON NUEVAS MEDIDAS

            String path = HttpContext.Current.Server.MapPath("~/imagenesArticulos"); //Path

            
            if (!System.IO.Directory.Exists(path))
            {
                System.IO.Directory.CreateDirectory(path); 

            }

            if (!System.IO.Directory.Exists(path + "/" + folderName))
            {
                System.IO.Directory.CreateDirectory(path + "/" + folderName); 
            }


            string nombreImagen = Guid.NewGuid().ToString("N") + ".jpg";

            

            string imgPath = Path.Combine(path + "\\" + folderName, nombreImagen);


            newImage.Save(imgPath, System.Drawing.Imaging.ImageFormat.Jpeg);

            NameValueCollection section = (NameValueCollection)ConfigurationManager.GetSection("appSettings");
            string hostIP = section["direccionIP"];

            return hostIP + "/imagenesArticulos/" + folderName + "/" + nombreImagen;

        }


        public string GuardarImagen( Image img, int size, string folderName )
        {
            if (size != 0)
            {
                if (img.Width > img.Height)
                {
                    decimal x = (decimal)size / (decimal)img.Height;

                    NewAncho = (int)(x * (decimal)img.Width);
                    NewAlto = size;


                }
                else
                {
                    decimal c = (decimal)size / (decimal)img.Width;

                    NewAncho = size;
                    NewAlto = (int)(c * (decimal)img.Height);

                }

            }
            else
            {
                NewAncho = img.Width;
                NewAlto = img.Height;
            }

            var newImage = new Bitmap(NewAncho, NewAlto);

            using (var g = Graphics.FromImage(newImage))
            {
                g.DrawImage(img, 0, 0, NewAncho, NewAlto);
            }

            // GUARDANDO IMAGEN CON NUEVAS MEDIDAS

            String path = HttpContext.Current.Server.MapPath("~/imagenesArticulos"); //Path


            if (!System.IO.Directory.Exists(path))
            {
                System.IO.Directory.CreateDirectory(path);

            }

            if (!System.IO.Directory.Exists(path + "/" + folderName))
            {
                System.IO.Directory.CreateDirectory(path + "/" + folderName);
            }


            string nombreImagen = Guid.NewGuid().ToString("N") + ".jpg";



            string imgPath = Path.Combine(path + "\\" + folderName, nombreImagen);


            newImage.Save(imgPath, System.Drawing.Imaging.ImageFormat.Jpeg);

            NameValueCollection section = (NameValueCollection)ConfigurationManager.GetSection("appSettings");
            string hostIP = section["direccionIP"];

            return hostIP+"/imagenesArticulos/" + folderName + "/" + nombreImagen;

        } 
    }
}