﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using webService.Models;
using System.Data;
using System.Drawing;
using System.Text;
using System.DirectoryServices;
using System.Management;
using System.Security.Principal;

namespace webService.libs
{
    public class Token
    {
        private DBECommerceContext db = new DBECommerceContext();
        public string CodigoToken { get; set; }

        public int ValidarToken( string token)
        {

            var tokenObtenido = db.sesions.Where(x => x.token == token).Select(x => x).Take(1).Distinct();

            if ( tokenObtenido.Count() == 1 )
            {
                return 1;
            } else
            {
                return 0;
            }
        }

        public int ValidarTokenAdmin(string token)
        {

            var tokenObtenido = db.admin_sesion.Where(x => x.token == token).Select(x => x).Take(1).Distinct();

            if (tokenObtenido.Count() == 1)
            {
                return 1;
            }
            else
            {
                return 0;
            }
        }

        public string GenerarTokenAdmin()
        {
            int numero = 150;
            var linea = "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";
            var longitud = linea.Length;
            var codigo = string.Empty;
            int indice;
            string txt = string.Empty;
            Random rnd = new Random();
            string tokenFinal = string.Empty;

            for (var i = 0; i < numero; i++)
            {
                indice = (int)(rnd.NextDouble() * longitud);
                txt = linea.Substring(indice, 1);
                tokenFinal += txt;
            }
                
              return tokenFinal;
        }

        public string GenerarToken()
        {
            int numero = 150;
            var linea = "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";
            var longitud = linea.Length;
            var codigo = string.Empty;
            int indice;
            string txt = string.Empty;
            Random rnd = new Random();
            string tokenFinal = string.Empty;

            for (var i = 0; i < numero; i++)
            {
                indice = (int)(rnd.NextDouble() * longitud) ;
                txt = linea.Substring(indice,1);
                tokenFinal += txt;
            }
            var validarNoDobleToken = db.sesions.Where(x => x.token == tokenFinal).Select(x => x.token).Take(1).Count();

            if (validarNoDobleToken == 0)
            {
                return tokenFinal;

            } else
            {
                for (var i = 0; i < numero; i++)
                {
                    indice = (int)(rnd.NextDouble() * longitud);
                    txt = linea.Substring(indice, 1);
                    tokenFinal += txt;
                }
                return tokenFinal;
            }
        }

        public int IniciarSesion( string usuario, string password )
        {
            string dominio, user, pass;

            dominio = "10.19.150.3";
            user = usuario;
            pass = password;

            //Aquí va el path URL del servicio de directorio LDAP
            string path = "LDAP://10.19.150.3";

            if (estaAutenticado(dominio, user, pass, path) == true)
            {
                //"Autenticado en LDAP!";
                return 1;
            }
            else
            {
                //"Error al Autenticar"
                return 0 ;
            }
        }

        private bool estaAutenticado(string dominio, string usuario, string pwd, string path)
        {
            //Armamos la cadena completa de dominio y usuario
            string domainAndUsername = dominio + @"\" + usuario;
            //Creamos un objeto DirectoryEntry al cual le pasamos el URL, dominio/usuario y la contraseña
            DirectoryEntry entry = new DirectoryEntry(path, domainAndUsername, pwd);
            try
            {
                DirectorySearcher search = new DirectorySearcher(entry);
                //Verificamos que los datos de logeo proporcionados son correctos
                SearchResult result = search.FindOne();
                if (result == null)
                {
                    return false;
                }
                else
                {
                    return true;
                }
            }
            catch (Exception ex)
            {
                
                return false;
            }
        }
    }
}