﻿using Microsoft.Ajax.Utilities;
using OfficeOpenXml;
using OfficeOpenXml.Style;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Configuration;
using System.Data;
using System.Data.Entity.Core.EntityClient;
using System.Data.Entity.Core.Objects;
using System.Data.Entity.Infrastructure;
using System.Data.SqlClient;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Web;
using System.Web.Http;
using System.Web.UI;
using System.Web.UI.WebControls;
using webService.libs;
using webService.Models;
namespace webService.Controllers
{
    public class AdminReportesController : ApiController
    {
        private DBECommerceContext db = new DBECommerceContext();
        private Token CodTokenValidator = new Token();
      
        [Route("admin/indicadores/{token}")]
        [HttpGet]
        public IHttpActionResult CargarIndicadores(string token)
        {
            if (CodTokenValidator.ValidarTokenAdmin(token) == 1)
            {
                var indicadores = db.indicadors.Select( x => new { id = x.idIndicador , descripcion = x.descripcion } );
                return Ok(indicadores);

            } else
            {
                var errorLogueo = new { status = 0, mensaje = "Porfavor inicie sesión." };
                return Ok(errorLogueo);
            }
        }

        [Route("admin/reporte/descarga")]
        [HttpPost]
        public IHttpActionResult GenerarReporte( AdminReporte reporte )
        {
            if ( CodTokenValidator.ValidarTokenAdmin(reporte.token) == 1 )
            {
                var agencia = db.VW_ECommerceAgencias.Where(x => x.nCodAge == reporte.codAgencia).Select(x => x.cAgencia).Take(1).FirstOrDefault();
                var error = new { status = 0, mensaje = " No se pudo generar el documento." };
                var noRecord = new { status = 0, mensaje = " No se encontraron registros " };

                var fltr = String.Empty;
                #region indicador 1
                if (reporte.indicador == 1)
                {
                    if (reporte.codAgencia == 0)
                    {
                        fltr = " WHERE fecha >= '" + reporte.inicio + "' AND fecha <= '" + reporte.fin + "' ";
                    }
                    else
                    {
                        fltr = " WHERE fecha >= '" + reporte.inicio + "' AND fecha <= '" + reporte.fin + "' AND agencia = '" + agencia + "'";
                    }


                    try
                    {
                        var registros = db.Database
                      .SqlQuery<ReporteArticuloMasVisto>("SELECT  " +
                                                        "MIN(A.[cSKU]) AS cSKU, " +
                                                        "MIN(A.[nombre]) AS nombre, " +
                                                        "MIN(B.[cMarca]) AS marca, " +
                                                        "MIN(A.[descripcion]) AS descripcion, " +
                                                        "MIN(A.[precioBase]) AS precioBase," +
                                                        "MIN(A.[precioVenta]) AS precioVenta, " +
                                                        "MIN(A.fecha) AS fecha, " +
                                                        "MIN(A.[agencia]) AS agencia, " +
                                                        "COUNT(*) total " +
                                                        "FROM [PER].[articulo_visto] AS A " +
                                                        "JOIN [PER].[reflejoBaseVenta] AS B ON A.cSKU = B.cSKU " +
                                                        fltr +
                                                        "GROUP BY A.cSKU " +
                                                        "HAVING COUNT(A.cSKU) > 0 " +
                                                        "ORDER BY COUNT(A.cSKU) DESC ");

                        if (registros.Count() > 0)
                        {
                            #region excelcode
                            ExcelPackage pck = new ExcelPackage();

                            //Create the worksheet 
                            ExcelWorksheet ws = pck.Workbook.Worksheets.Add("Más vistos");

                            // Sets Headers
                            ws.Cells[1, 1].Value = "SKU";
                            ws.Cells[1, 2].Value = "NOMBRE";
                            ws.Cells[1, 3].Value = "MARCA";
                            ws.Cells[1, 4].Value = "DESCRIPCION";
                            ws.Cells[1, 5].Value = "PRECIO BASE";
                            ws.Cells[1, 6].Value = "PRECIO VENTA";
                            ws.Cells[1, 7].Value = "AGENCIA";
                            ws.Cells[1, 8].Value = "VISTAS";

                            // Inserts Data
                            var i = 0;

                            foreach (var row in registros)
                            {
                                i++;
                                ws.Cells[i + 1, 1].Value = row.cSKU;
                                ws.Cells[i + 1, 2].Value = row.nombre;
                                ws.Cells[i + 1, 3].Value = row.marca;
                                ws.Cells[i + 1, 4].Value = row.descripcion;
                                ws.Cells[i + 1, 5].Value = row.precioBase;
                                ws.Cells[i + 1, 6].Value = row.precioVenta;
                                ws.Cells[i + 1, 7].Value = row.agencia;
                                ws.Cells[i + 1, 8].Value = row.total;
                            }

                            // Format Header of Table
                            using (ExcelRange rng = ws.Cells["A1:I1"])
                            {

                                rng.Style.Font.Bold = true;
                                rng.Style.Fill.PatternType = ExcelFillStyle.Solid; //Set Pattern for the background to Solid 
                                rng.Style.Fill.BackgroundColor.SetColor(Color.Blue); //Set color to DarkGray 
                                rng.Style.Font.Color.SetColor(Color.White);
                            }
                            ws.Column(1).AutoFit();
                            ws.Column(2).AutoFit();
                            ws.Column(3).AutoFit();
                            ws.Column(5).AutoFit();
                            ws.Column(6).AutoFit();
                            ws.Column(7).AutoFit();
                            ws.Column(8).AutoFit();
                            #endregion
                            byte[] data = pck.GetAsByteArray();
                            var exitoso = new { status = 1, link = GuardarExcel(data) };
                            return Ok(exitoso);

                        }
                        else
                        {
                            return Ok(noRecord);
                        }

                    }
                    catch (Exception e )
                    {

                        return Ok(error); ;
                    }
                  

                }
                #endregion
                #region indicador 2
                else if (reporte.indicador == 2)
                {
                    fltr = " WHERE  A.fechaCreacion BETWEEN '" + reporte.inicio + "' AND '" + reporte.fin + "' AND B.codAgencia = " + reporte.codAgencia + " ";

                    if (reporte.codAgencia == 0)
                    {
                        fltr = " WHERE A.fechaCreacion BETWEEN '" + reporte.inicio + "' AND '" + reporte.fin + "' ";
                    }
                    

                    try
                    {
                        var usuarios = db.Database
                        .SqlQuery<ReporteUsuarioRegistrado>("SELECT MIN(A.nombres) AS nombres , " +
                                                            "MIN(A.apellidos) AS apellidos, " +
                                                            "MIN(A.dni) AS dni, " +
                                                            "MIN(A.correo) AS correo, " +
                                                            "MIN(A.telefono) AS telefono, " +
                                                            "MIN(A.fechaCreacion) AS fechaRegistro, " +
                                                            "MIN(C.[cAgencia]) AS agencia " +
                                                            "FROM [PER].[usuarios] A " +
                                                            "LEFT JOIN [PER].[sesion] B ON  A.idSesion = B.idSesion " +
                                                            "LEFT JOIN [PER].[VW_ECommerceAgencias] C ON B.[codAgencia] = C.[nCodAge] " +
                                                            fltr  +
                                                            "GROUP BY DNI ");

                        if (usuarios.Count() > 0)
                        {
                            ExcelPackage pck = new ExcelPackage();

                            //Create the worksheet 
                            ExcelWorksheet ws = pck.Workbook.Worksheets.Add("Sheet 1");

                            // Sets Headers
                            ws.Cells[1, 1].Value = "NOMBRES";
                            ws.Cells[1, 2].Value = "APELLIDOS";
                            ws.Cells[1, 3].Value = "DNI";
                            ws.Cells[1, 4].Value = "CORREO";
                            ws.Cells[1, 5].Value = "TELEFONO";
                            ws.Cells[1, 6].Value = "FECHA DE REGISTRO";
                            ws.Cells[1, 7].Value = "AGENCIA";

                            // Inserts Data
                            var i = 0;

                            foreach (var row in usuarios)
                            {
                                i++;
                                ws.Cells[i + 1, 1].Value = row.nombres;
                                ws.Cells[i + 1, 2].Value = row.apellidos;
                                ws.Cells[i + 1, 3].Value = row.dni;
                                ws.Cells[i + 1, 4].Value = row.correo;
                                ws.Cells[i + 1, 5].Value = row.telefono;
                                ws.Cells[i + 1, 6].Value = row.fechaRegistro;
                                ws.Cells[i + 1, 7].Value = row.agencia;
                            }

                            // Format Header of Table
                            using (ExcelRange rng = ws.Cells["A1:G1"])
                            {

                                rng.Style.Font.Bold = true;
                                rng.Style.Fill.PatternType = ExcelFillStyle.Solid; //Set Pattern for the background to Solid 
                                rng.Style.Fill.BackgroundColor.SetColor(Color.Blue); //Set color to DarkGray 
                                rng.Style.Font.Color.SetColor(Color.White);
                            }

                            ws.Column(6).Style.Numberformat.Format = "dd/MM/yyyy HH:mm";
                            ws.Column(1).AutoFit();
                            ws.Column(2).AutoFit();
                            ws.Column(4).AutoFit();
                            ws.Column(5).AutoFit();
                            ws.Column(6).AutoFit();
                            ws.Column(7).AutoFit();

                            byte[] data = pck.GetAsByteArray();

                            var exitoso = new { status = 1, link = GuardarExcel(data) };
                            return Ok(exitoso);

                        }
                        else
                        {
                            return Ok(noRecord);
                        }
                    }
                    catch (Exception e)
                    {

                        return Ok(error);
                    }

                }
                #endregion
                #region INDICADOR 3
                else if (reporte.indicador == 3)
                {
                    fltr = " WHERE  A.fecha BETWEEN '" + reporte.inicio + "' AND '" + reporte.fin + "' AND A.codAgencia = " + reporte.codAgencia + " ";

                    if (reporte.codAgencia == 0)
                    {
                        fltr = " WHERE A.fecha BETWEEN '" + reporte.inicio + "' AND '" + reporte.fin + "' ";
                    }

                    try
                    {
                        var articulosSeparados = db.Database
                           .SqlQuery<ReporteArticuloReservado>(" SELECT A.[cSKU] AS sku, " +
                                                               " A.[nombre] AS articulo, " +
                                                               " E.[cMarca] AS marca, "+
                                                               " A.[descripcion] AS descripcionArticulo, " +
                                                               " A.[precioVenta] AS precioVenta, " +
                                                               " A.dni AS dni, " +
                                                               " C.cNombres AS nombre, " +
                                                               " C.cApellidos AS apellidos, " +
                                                               " A.correo AS correo, " +
                                                               " A.telefono AS celular, " +
                                                               " A.fecha AS fechaReserva, " +
                                                               " A.codAgencia AS codAgencia, " +
                                                               " D.[cAgencia] AS nombreAgencia, " +
                                                               " IIF(B.[dni] IS NULL, 'SI', 'NO') AS cliente " +
                                                               " FROM [PER].[articulo_separado] AS A " +
                                                               " LEFT JOIN [PER].[usuarios] B ON B.[dni] = A.[dni] " +
                                                               " LEFT JOIN [PER].[VW_ECommerceClientes] AS C ON C.[cDNI] = A.[dni] " +
                                                               " LEFT JOIN [PER].[VW_ECommerceAgencias] AS D ON D.[nCodAge] = A.[codAgencia]  " +
                                                               " LEFT JOIN [PER].[reflejoBaseVenta] AS E ON A.cSKU = E.cSKU "+
                                                                 fltr);

                        if (articulosSeparados.Count() > 0)
                        {
                            ExcelPackage pck = new ExcelPackage();

                            //Create the worksheet 
                            ExcelWorksheet ws = pck.Workbook.Worksheets.Add("Sheet 1");

                            // Sets Headers
                            ws.Cells[1, 1].Value = "SKU";
                            ws.Cells[1, 2].Value = "ARTÍCULO";
                            ws.Cells[1, 3].Value = "MARCA";
                            ws.Cells[1, 4].Value = "DESCRIPCIÓN";
                            ws.Cells[1, 5].Value = "PRECIO VENTA";
                            ws.Cells[1, 6].Value = "DNI";
                            ws.Cells[1, 7].Value = "NOMBRES";
                            ws.Cells[1, 8].Value = "APELLIDOS";
                            ws.Cells[1, 9].Value = "CORREO";
                            ws.Cells[1, 10].Value = "CELULAR";
                            ws.Cells[1, 11].Value = "FECHA DE RESERVA";
                            ws.Cells[1, 12].Value = "CÓDIGO DE AGENCIA";
                            ws.Cells[1, 13].Value = "NOMBRE DE AGENCIA";
                            ws.Cells[1, 14].Value = "CLIENTE";
                            // Inserts Data
                            var i = 0;

                            foreach (var row in articulosSeparados)
                            {
                                i++;
                                ws.Cells[i + 1, 1].Value = row.sku;
                                ws.Cells[i + 1, 2].Value = row.articulo;
                                ws.Cells[i + 1, 3].Value = row.marca;
                                ws.Cells[i + 1, 4].Value = row.descripcionArticulo;
                                ws.Cells[i + 1, 5].Value = row.precioVenta;
                                ws.Cells[i + 1, 6].Value = row.dni;
                                ws.Cells[i + 1, 7].Value = row.nombre;
                                ws.Cells[i + 1, 8].Value = row.apellidos;
                                ws.Cells[i + 1, 9].Value = row.correo;
                                ws.Cells[i + 1, 10].Value = row.celular;
                                ws.Cells[i + 1, 11].Value = row.fechaReserva;
                                ws.Cells[i + 1, 12].Value = row.codAgencia;
                                ws.Cells[i + 1, 13].Value = row.nombreAgencia;
                                ws.Cells[i + 1, 14].Value = row.cliente;
                            }

                            // Format Header of Table
                            using (ExcelRange rng = ws.Cells["A1:N1"])
                            {

                                rng.Style.Font.Bold = true;
                                rng.Style.Fill.PatternType = ExcelFillStyle.Solid; //Set Pattern for the background to Solid 
                                rng.Style.Fill.BackgroundColor.SetColor(Color.Blue); //Set color to DarkGray 
                                rng.Style.Font.Color.SetColor(Color.White);
                            }

                            ws.Column(11).Style.Numberformat.Format = "dd/MM/yyyy HH:mm";
                            ws.Column(1).AutoFit();
                            ws.Column(2).AutoFit();
                            ws.Column(3).AutoFit();
                            ws.Column(5).AutoFit();
                            ws.Column(6).AutoFit();
                            ws.Column(7).AutoFit();
                            ws.Column(8).AutoFit();
                            ws.Column(9).AutoFit();
                            ws.Column(10).AutoFit();
                            ws.Column(11).AutoFit();
                            ws.Column(12).AutoFit();
                            ws.Column(13).AutoFit();
                            ws.Column(14).AutoFit();

                            byte[] data = pck.GetAsByteArray();

                            var exitoso = new { status = 1, link = GuardarExcel(data) };
                            return Ok(exitoso);
                            #endregion
                        }
                        else
                        {
                            return Ok(noRecord);
                        }
                    }
                    catch (Exception e)
                    {

                        return Ok(e);
                    }

                }

                return Ok(error);

            } else
            {
                var errorLogueo = new { status = 0, mensaje = "Porfavor inicie sesión." };
                return Ok(errorLogueo);
            }
            
        }


        public string GuardarExcel( byte[] archivo )
        {
            String path = HttpContext.Current.Server.MapPath("~/reportes"); //Path  

            var anio = DateTime.Now.Year.ToString(); 
            var mes = DateTime.Now.Month.ToString();
            var fecha = DateTime.Now.Day.ToString();

            if (!System.IO.Directory.Exists(path))
            {
                System.IO.Directory.CreateDirectory(path);

            }

            if ( !System.IO.Directory.Exists(path + "/" + anio ) )
            {
                System.IO.Directory.CreateDirectory (path + "/" + anio);
            }

            if (!System.IO.Directory.Exists(path + "/" + anio+"/"+ mes ))
            {
                System.IO.Directory.CreateDirectory( path + "/" + anio+"/"+ mes );
            }

            if (!System.IO.Directory.Exists(path + "/" + anio + "/" + mes+"/"+fecha))
            {
                System.IO.Directory.CreateDirectory(path + "/" + anio + "/" + mes+"/"+fecha);
            }

            string nombreArchivo = Guid.NewGuid().ToString("N") + ".xlsx";

            string excelPath = Path.Combine( path + "\\" + anio + "\\" + mes + "\\" + fecha , nombreArchivo );

            File.WriteAllBytes(excelPath, archivo);

            NameValueCollection section = (NameValueCollection)ConfigurationManager.GetSection("appSettings");
            string hostIP = section["direccionIP"];

            return hostIP + "/reportes/" + anio + "/" + mes + "/"+ fecha +"/"+ nombreArchivo; 
        }

    }
}
