﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using webService.libs;
using webService.Models;

namespace webService.Controllers
{
    public class servicioSMSController : ApiController
    {
        private DBECommerceContext db = new DBECommerceContext();
        private Token CodTokenValidator = new Token();

        [Route("api/enviarsms/")]
        [HttpPost]
        public IHttpActionResult EnviarSMS(MoldeEnvioSMS datos)
        {
            if (CodTokenValidator.ValidarToken(datos.token) == 1)
            {
                //credenciales del servicio sms
                var credenciales = db.servicioSMS.Take(1).Single();
                var usuario = credenciales.usuario;
                var clave = credenciales.clave;


                var cliente = db.VW_ECommerceClientes.Where(x => x.cDNI == datos.dni).Select(x => x).Take(1).FirstOrDefault();
                var codAge = db.reflejoBaseVentas.Where(x => x.cSKU == datos.sku).Select(x => x.nCodAge).Single();
                var agencia = db.VW_ECommerceAgencias.Where(x => x.nCodAge == codAge ).Select(x => x).FirstOrDefault();
                var nombreArticulo = db.reflejoBaseVentas.Where(x => x.cSKU == datos.sku ).Select(x => x.cArticulo).Single();

                string[] mesSpa = new string[] { "Enero", "Febrero", "Marzo" , "Abril", "Mayo", "Junio", "Julio","Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre" };


               // var TiempoEspera = db.tiemposReservas.Where(x => x.ubicacion == agencia.cDepartamento).Select(x => x.tiempoReserva).Take(1).Single();

                var fechaRecojo = db.articulo_separado
                    .Where(x => x.cSKU == datos.sku)
                    .Select(x => x.fechaRecojo)
                    .First();

                string mensaje1 = "INVERSIONES LA CRUZ S.A. "+ nombreArticulo + ". Cod. " + datos.sku + " Hasta: " + fechaRecojo ;
                string mensaje2 = " En: Agencia " + agencia.cAgencia + " Dir: " + agencia.cDireccion;

               // var url = credenciales.url + "?usr=" + usuario + "&pas=" + clave + "&num=51" + datos.telefono + "&msg=" + mensaje1;

                ServicioSMS.wsSMS sms = new ServicioSMS.wsSMS();
                var validar1 =  sms.EnviarMensaje(datos.telefono,mensaje1);

                if ( validar1 == true )
                {
                    var validar2 = sms.EnviarMensaje(datos.telefono, mensaje2);
                    if(validar2 == true)
                    {
                         return Ok( new { status = 1 , mensaje = "envío exitoso" } );

                    }else
                    {
                        return Ok(new { status = 0, mensaje = "Error" });
                    }
                }
                else
                {
                    return Ok(new { status = 0, mensaje = "Error" });
                }

               // EnviarSMS(url);

            

              /*  if( TiempoEspera != null && TiempoEspera > 0 )
                {   
                  //  DateTime fechaSpera = DateTime.Now.AddHours((int)TiempoEspera);

                   // var anio = fechaSpera.Year.ToString();
                   // var mes = fechaSpera.Month;
                   // var fecha = fechaSpera.Day.ToString();

                } else
                {
                    return Ok(new { status = 0, mensaje =" Error"});
                } */

            } else
            {
                var errorValidacion = new { status = 0, mensaje = "Porfavor inicie sesión." };

                return Ok(errorValidacion);
            }
                

        }

        public void EnviarSMS( string url )
        {
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(url);
            request.AutomaticDecompression = DecompressionMethods.GZip;

            using (HttpWebResponse response = (HttpWebResponse)request.GetResponse())
            using (Stream stream = response.GetResponseStream())
            using (StreamReader reader = new StreamReader(stream))

            {
                var respuesta = reader.ReadToEnd();

            }
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool servicioSMSExists(int id)
        {
            return db.servicioSMS.Count(e => e.id == id) > 0;
        }
    }
}