﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using webService.libs;
using webService.Models;

namespace webService.Controllers
{
    public class VW_ECommerceBaseVentaController : ApiController
    {
        private DBECommerceContext db = new DBECommerceContext();
        private Token CodTokenValidator = new Token();

        [Route("api/producto/{codToken}/{sku}")]
        [HttpGet]
        public IHttpActionResult ObtenerDetalleProducto(string codToken, string sku)
        {
            if (CodTokenValidator.ValidarToken(codToken) == 1 )
            {
                articulo_visto record = new articulo_visto();
                var imagenes = db.articuloImagens.Where(x => x.sku == sku && x.activo == true ).Select(x =>
                new
                {   
                    idImgs = x.id ,
                    imgSmall = x.imgSizeSmall,
                    imgMediun = x.imgSizeMediun,
                    imgOriginal = x.imgSizeOriginal,
                    imgPred = x.predeterminado

                }).OrderBy( x => x.imgPred).Take(5);
                
                var CodAgencia = db.reflejoBaseVentas.Where(x => x.cSKU == sku).Select(x => x.nCodAge).Take(1).Single();
               
                var agencia = db.VW_ECommerceAgencias.Where(x => x.nCodAge == CodAgencia).Select(x => x).Take(1);
                
                var dpto = agencia.FirstOrDefault().cDepartamento;

                var tiempoEspera = db.tiemposReservas.Where(x => x.ubicacion == dpto)
                    .Select( x => x.tiempoReserva ).Take(1).Single();

                var diasEspera = db.tiemposReservas.Where(x => x.ubicacion == dpto)
                   .Select(x => x.dias).Take(1).Single();

                var descripcion = db.reflejoBaseVentas.Where(x => x.cSKU == sku && x.estado == 1 ).Select(x => new
               {    
                   nombre = x.cArticulo,
                   descripcion = x.cDescripcion,
                   imagenes = imagenes,
                   marca = x.cMarca,
                   precioNormal = x.nPrecioBase,
                   precioVenta = x.nPrecioVenta,
                   datosAgencia = agencia,
                   tiempoDeEspera = tiempoEspera,
                   observaciones = x.cObservaciones
                   
               }).Take(1);


                record.cSKU = sku;
                record.nombre = descripcion.Select(x => x.nombre).FirstOrDefault();
                record.descripcion = descripcion.Select(x => x.descripcion).FirstOrDefault();
                record.precioBase = descripcion.Select(x => x.precioNormal).FirstOrDefault();
                record.precioVenta = descripcion.Select(x => x.precioVenta).FirstOrDefault();
                record.fecha = DateTime.Now;
                record.agencia = agencia.FirstOrDefault().cAgencia;
                db.articulo_visto.Add(record);

                try
                {
                   
                    db.SaveChanges();
                }

                catch (Exception e)
                {
                   
                }
                var idImgPredeterminada = db.articuloImagens.Where(x => x.sku == sku && x.activo == true && x.predeterminado == true)
                    .Select( x => x.id ).Take(1).SingleOrDefault();

                return Ok(descripcion.Select( x => new {
                   
                        descripcion = x.descripcion,
                        imagenes = imagenes,
                        marca = x.marca,
                        datosAgencia = agencia,
                        tiempoDeEspera = tiempoEspera,
                        diasReserva = diasEspera,
                        observaciones = x.observaciones,
                        idImgPredeterminada = idImgPredeterminada
                    } ));
               
            } else
            {
                var errorValidacion = new { status = 0, mensaje = "Porfavor inicie sesión." };

                return Ok(errorValidacion);
            }
           

        }

        
        [Route("api/productos/")]
        [HttpPost]
        public IHttpActionResult FiltrarProducto( MoldePeticionFiltro peticion )
        {
            if ( CodTokenValidator.ValidarToken( peticion.token ) == 1 )
            {
               var idSesion = db.sesions.Where(x => x.token == peticion.token).Select(x => x.idSesion).Take(1).Single();


                navegacion registroBusqueda = new navegacion();
                registroBusqueda.idSesion = idSesion;
                registroBusqueda.nCodFamilia = peticion.familia;
                registroBusqueda.cMarca = peticion.marca;
                registroBusqueda.fechaConsulta = DateTime.Now;

                if ( peticion.rango == 0 )
                {
                    registroBusqueda.nIdRango = null;
                     
                } else
                {
                    registroBusqueda.nIdRango = registroBusqueda.nIdRango;
                }

                db.navegacions.Add(registroBusqueda);

                try
                {
                    db.SaveChanges();
                }
                catch (DbUpdateException)
                {

                    throw;

                }


                List<string> arrayFiltros = new List<string>();
                
                if ( peticion.familia != 0 )
                {
                    arrayFiltros.Add(" A.[nCodFamilia] = " + peticion.familia);
                }

                if (peticion.codAgencia != 0)
                {
                    arrayFiltros.Add("A.[nCodAge] = " + peticion.codAgencia);
                }
                if (peticion.rango != 0 )
                {
                    var rango = db.rangoPrecios.Where(x => x.nIdRango == peticion.rango).Select(x => new
                    {
                        varlorMin = x.cValorMinimo,
                        varlorMax = x.cValorMaximo

                    }).Take(1);

                    int? valorMin;
                    int? valorMax;

                    if (rango.Count() == 0)
                    {
                        valorMin = null;
                        valorMax = null;

                    }
                    else
                    {
                        valorMin = rango.Single().varlorMin;
                        valorMax = rango.Single().varlorMax;
                        arrayFiltros.Add(" A.[nPrecioVenta] BETWEEN " + valorMin + " AND " + valorMax);
                    }
                }
                if (peticion.marca != "0")
                {
                    arrayFiltros.Add("A.[cMarca] = '" + peticion.marca + "'");
                }

                string filtro = " AND ";
                String filtro2 = String.Join(" AND ", arrayFiltros.ToArray());

                if ( filtro2.Length > 0 )
                {
                    filtro += filtro2;
                }
                else
                {
                    filtro = " ";
                }
                 

               
                var productosBaseVenta = db.Database
                    .SqlQuery<FiltroProducto>("SELECT A.[cSKU] AS sku , MIN(A.[cArticulo]) AS nombre , MIN(B.imgSizeSmall) AS imagen , MIN(A.[nPrecioBase]) AS precioNormal, " +
                                            "MIN(A.[nPrecioVenta]) AS precioVenta " +
                                            "FROM [PER].[reflejoBaseVenta] A " +
                                            "LEFT JOIN [PER].[articuloImagen] B ON [cSKU] = [sku] " +
                                            "WHERE [cSKU] IS NOT NULL AND B.imgSizeSmall IS NOT NULL  AND  A.[estado] = 1 AND  ( B.[predeterminado] IS NULL OR B.[predeterminado] = 1 )  " + filtro +
                                            "  GROUP BY A.[cSKU] ORDER BY precioVenta ");

                if (productosBaseVenta.Count() > 0 )
                {
                    return Ok(productosBaseVenta);

                } else
                {
                    var noHayPRoductos = new { status = 0, mensaje = "No se encontraron productos." };
                    return Ok(noHayPRoductos);
                }
                
            } else
            {
                var errorValidacion = new { status = 0, mensaje = "Porfavor inicie sesión." };

                return Ok(errorValidacion);
            }

        }

        
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool VW_ECommerceBaseVentaExists(int id)
        {
            return db.VW_ECommerceBaseVenta.Count(e => e.nIdArticulo == id) > 0;
        }
    }
}