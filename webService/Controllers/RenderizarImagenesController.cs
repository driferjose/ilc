﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Configuration;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using webService.libs;
using webService.Models;

namespace webService.Controllers
{
    public class RenderizarImagenesController : ApiController
    {
        private DBECommerceContext db = new DBECommerceContext();
        private Token CodTokenValidator = new Token();

        [Route("api/generar")]
        [HttpGet]
        public int RenderizarImagenes()
        {
            
            var imgNull = db.Database.SqlQuery<ImgNull>("SELECT cSKU FROM [PER].[VW_ECommerceBaseVenta]  WHERE "+
                                                        "oImagen1 IS  NULL AND oImagen2 IS  NULL AND  oImagen3 IS  NULL "+
                                                        "AND  oImagen4 IS  NULL AND  oImagen5 IS  NULL AND  oImagen6 IS  NULL");

            var sql = " SELECT TOP 1 A.* FROM [PER].[VW_ECommerceBaseVenta] A " +
                      " LEFT JOIN [PER].[articuloImagen] B ON A.[cSKU] = B.[sku] WHERE [sku] IS NULL ";

            if ( imgNull.Count() > 0 )
            {
                foreach (var sku in imgNull)
                {
                    sql += "AND A.[cSKU] != '"+ sku.cSKU + "' ";
                }
            }

            

            var validacion = db.VW_ECommerceBaseVenta.SqlQuery(sql);


            if ( validacion.Count() == 0 )
            {
                return 0 ;
            }
           

            var resultado = validacion.Single();



            List<byte[]> listaImagenes = new List<byte[]>();

            listaImagenes.Add(resultado.oImagen1);
            listaImagenes.Add(resultado.oImagen2);
            listaImagenes.Add(resultado.oImagen3);
            listaImagenes.Add(resultado.oImagen4);
            listaImagenes.Add(resultado.oImagen5);
            listaImagenes.Add(resultado.oImagen6);

           ;
            foreach (byte[] item in listaImagenes)
            {
                articuloImagen registro = new articuloImagen();

                var render = new Imagen();
                if (item != null)
                {
                    registro.imgSizeSmall = render.ScaleImage(item, 100, resultado.cSKU);
                    registro.imgSizeMediun = render.ScaleImage(item, 800, resultado.cSKU);
                    registro.imgSizeOriginal = render.ScaleImage(item, 0, resultado.cSKU);
                    registro.sku = resultado.cSKU;
                    registro.activo = true ;
                    db.articuloImagens.Add(registro);
                }
            
            }

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateException)
            {
                throw;
            }
            
            return 1 ;
        }
    }
}
