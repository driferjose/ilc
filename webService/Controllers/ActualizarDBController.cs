﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using webService.Models;

namespace webService.Controllers
{
    public class ActualizarDBController : ApiController
    {
        private DBECommerceContext db = new DBECommerceContext();
        
        [Route("actualizar/db/")]
        [HttpGet]
        public IHttpActionResult ActualizarDBreflejoBaseVenta()
        {
            try
            {
                db.Database.ExecuteSqlCommand(" INSERT INTO  [PER].[reflejoBaseVenta] ( [nIdArticulo], [cSKU], [cFamilia], [nCodFamilia], [nCodArticulo], [cArticulo], [nCodMarca] " +
                                               ",[cMarca],[nCodLinea], [cLinea], [nCodModelo], [cModelo], [cDescripcion], [nPrecioBase], [nPrecioVenta], [nCodAge], [cObservaciones], [activo]) " +
                                                "SELECT [nIdArticulo], [cSKU], [cFamilia],[nCodFamilia],[nCodArticulo], [cArticulo], [nCodMarca], [cMarca], [nCodLinea], [cLinea], [nCodModelo], "+
                                                " [cModelo], [cDescripcion], [nPrecioBase], [nPrecioVenta], b.[nCodAge], [cObservaciones], 1 " +
                                                "FROM [PER].[VW_ECommerceBaseVenta] B "+
                                                "JOIN [PER].[VW_ECommerceAgencias] C ON B.[nCodAge] = C.[nCodAge] "+
                                                "WHERE NOT EXISTS (SELECT [cSKU] FROM [PER].[reflejoBaseVenta] "+
                                                "WHERE [cSKU] = B.[cSKU])");


                db.Database.ExecuteSqlCommand(" DELETE FROM [PER].[reflejoBaseVenta] WHERE [cSKU] NOT IN ( SELECT [cSKU] FROM [PER].[VW_ECommerceBaseVenta] ) ");

            }
            catch (Exception e)
            {
                var error = new { status = 0, mensaje = "No se pudo actualizar la base de datos."  };
                return Ok(error);
            }

            var success = new { status = 1, mensaje = "Actualizado con exito" };
            return Ok(success); 
        }
    }
}
