﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Configuration;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Core.Objects;
using System.Data.Entity.Infrastructure;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Mail;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Web;
using System.Web.Hosting;
using System.Web.Http;
using System.Web.Http.Description;
using System.Web.UI.WebControls;
using webService.libs;
using webService.Models;

namespace webService.Controllers
{   
    public class usuariosController : ApiController
    {
        private DBECommerceContext db = new DBECommerceContext();
        private Token CodTokenValidator = new Token();

        [Route("api/login/")]
        [HttpPost]
        public IHttpActionResult IniciarSesion( MoldeLogueo datosLogueo )
        {
            var validarSesion = CodTokenValidator.IniciarSesion( datosLogueo.usuario, datosLogueo.password );

            if(validarSesion == 0)
            {
                var error = new { status = 0, mensaje = "El usuario o la contraseña son incorrectos." };
                return Ok(error);
            } else
            {
                var codigoAgencia = db.Database
                    .SqlQuery<codAge>
                    ("SELECT  [per].[FN_ECommerceDevuelveAgenciaUsuario]('"+ datosLogueo.usuario + "') AS codAgencia ;")
                    .SingleOrDefault();

                sesion nuevaSesion = new sesion();
                nuevaSesion.token = CodTokenValidator.GenerarToken();
                nuevaSesion.usuario = datosLogueo.usuario;
                nuevaSesion.codAgencia = codigoAgencia.codAgencia;
                nuevaSesion.fechaCreacion = DateTime.Now;
                nuevaSesion.activo = true;

                db.sesions.Add( nuevaSesion );

                try
                {
                    db.SaveChanges();
                }
                catch (DbUpdateException)
                {
                    var error = new { status = 0, mensaje = "Error" };
                    return Ok(error);
                }

                var succesful = new { status = 1 , mensaje = "Inicio de sesion Exitoso." , token = nuevaSesion.token };

                return Ok(succesful);

            }
        }
        [Route("api/logout/")]
        [HttpPost]
        public IHttpActionResult CerrarSesion( MoldeLogout datosSalida )
        {
            var validarSesion = CodTokenValidator.IniciarSesion(datosSalida.usuario, datosSalida.password);
                    
            if (validarSesion == 0)
            {
                var error = new { status = 0, mensaje = "El usuario o la contraseña son incorrectos." };
                return Ok(error);
            }
            else
            {
                
                db.Database.ExecuteSqlCommand(" UPDATE [PER].[sesion] SET [fechaSalida] = '"+DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss.fffffff") +"', [usuarioSalida] = '"+datosSalida.usuario+"'  WHERE [token] = '"+datosSalida.token+"'; ");

                try
                {
                    db.SaveChanges();
                }
                catch (DbUpdateException)
                {
                    var error = new { status = 0, mensaje = "Error" };
                    return Ok(error);
                }
                    
                return Ok(new { status = 1, mensaje = "Exitoso"});

                
                
            }
        }
        

        [Route("api/registrar/")]
        [HttpPost]
        public IHttpActionResult RegistrarNuevoUsuario( RegistroUsuario user )
        {
            if (CodTokenValidator.ValidarToken(user.token) == 1)
            {
                var validar = db.VW_ECommerceClientes.Where(x => x.cDNI == user.dni).Distinct().Count();

                if (validar > 0)
                {
                    var error = new { status = 0, mensaje = "Usuario ya registrádo." };
                    return Ok(error);
                }

                var errorCampos = new { status = 0, mensaje = "Uno o más campos tienen información incorrecta." };

                string patronNombre = @"^[a-zA-Z\s]+$";
                Match validadrNombre = Regex.Match(user.nombres, patronNombre);

                string patronApellido = @"^[a-zA-Z\s]+$";
                Match validarApellido = Regex.Match(user.apellidos, patronApellido);

                string PatronDni = @"^[0-9]{8}$";
                Match validarDni = Regex.Match(user.dni, PatronDni);

                string PatronEmail = @"^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$";
                Match validarEmail = Regex.Match(user.correo, PatronEmail);

                string PatronTelefono = @"^[9][0-9]{8}$";
                Match validarTelefono = Regex.Match(user.telefono, PatronTelefono);

                
                if (!validarEmail.Success || !validadrNombre.Success || !validarApellido.Success || !validarDni.Success || !validarTelefono.Success)
                {
                    return Ok(errorCampos);
                }

                try
                {
                    var dbContextDiagram = new DBECommerceEntities1();

                    ObjectParameter salida1 = new ObjectParameter("nResul", typeof(int));
                    ObjectParameter salida2 = new ObjectParameter("sResul", typeof(string));

                    var resultado = dbContextDiagram
                        .SP_ECommerceRegistrarCliente(user.dni,
                                                    user.nombres.ToUpper(),
                                                    user.apellidos.ToUpper(),
                                                    user.correo,
                                                    user.telefono,
                                                    salida1,
                                                    salida2);
                    if ( (int)salida1.Value == 0 )
                    {
                        return Ok(new { status = 0 , mensaje = salida2.Value });
                    }

                }
                catch (Exception)
                {

                    throw;
                }

               
                  
                var cliente = db.VW_ECommerceClientes.Where(x => x.cDNI == user.dni)
                    .Select(x => x).Take(1)
                    .Single();

                var idSesion = db.sesions.Where(x => x.token == user.token)
                    .Select(x => x.idSesion).Take(1)
                    .Single();


                usuarios reserva = new usuarios();
                reserva.nombres = user.nombres;
                reserva.apellidos = user.apellidos;
                reserva.dni = user.dni;
                reserva.telefono = user.telefono;
                reserva.correo = user.correo;
                reserva.fechaCreacion = DateTime.Now;
                reserva.idSesion = idSesion;
                
                db.usuarios.Add(reserva);

                try
                {
                    db.SaveChanges();
                }
                catch (DbUpdateException  )

                {
                    return Ok();
                }


                var exitoso = new { status = 1, mensaje = "Registro exitóso" };

                return Ok(exitoso);

            } else
            {
                var errorValidacion = new { status = 0, mensaje = "Porfavor inicie sesión." };

                return Ok(errorValidacion);
            }
           
        }

        [Route("api/separar/")]
        public IHttpActionResult Postusuario_producto(MoldeSeparacion moldeSeparacion)
        {
            if (CodTokenValidator.ValidarToken(moldeSeparacion.token) == 1)
            {
                string PatronDni = @"^[0-9]{8}$";
                Match validarDni = Regex.Match(moldeSeparacion.dni, PatronDni);

                string PatronEmail = @"^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$";
                Match validarEmail = Regex.Match(moldeSeparacion.correo, PatronEmail);

                string PatronTelefono = @"^[9][0-9]{8}$";
                Match validarTelefono = Regex.Match(moldeSeparacion.telefono, PatronTelefono);

                var errorCampos = new { status = 2 , mensaje = "Uno o más campos tienen información incorrecta." };

                if (!validarEmail.Success ||  !validarDni.Success || !validarTelefono.Success)
                {
                    return Ok(errorCampos);
                }

                var validarCliente = db.VW_ECommerceClientes.Where(x => x.cDNI == moldeSeparacion.dni).Select(x => x.cDNI);

                if (validarCliente.Count() == 0 )
                {
                    var errorCliente = new { status = 0 , mensaje = "Lo sentimos, por favor debe registrarse para poder reservar un producto." };
                    return Ok(errorCliente);
                }

                var validarSKU = db.VW_ECommerceBaseVenta.Where(x => x.cSKU == moldeSeparacion.cSKU).Select(x => x.cSKU).Take(1);

                if (validarSKU.Count() == 0)
                {

                    var error = new { status = 2 , mensaje = "Lo sentimos, el producto ya ha sido reservado" };

                    try
                    {
                        db.Database.ExecuteSqlCommand("UPDATE [PER].[reflejoBaseVenta] SET estado = 0  WHERE [cSKU] ='" + moldeSeparacion.cSKU + "'");

                    }
                    catch
                    {

                    }

                    return Ok(error);
                }

                var codAgencia = db.VW_ECommerceBaseVenta
                    .Where(x => x.cSKU == moldeSeparacion.cSKU)
                    .Select(x => x.nCodAge)
                    .Take(1)
                    .Single();

                 var agencia = db.VW_ECommerceAgencias
                    .Where(x => x.nCodAge == codAgencia)
                    .Select(x => x)
                    .Take(1)
                    .Single();


                var articulo = db.VW_ECommerceBaseVenta
                    .Where(x => x.cSKU == moldeSeparacion.cSKU)
                    .Select(x => new
                        { nombre = x.cArticulo,
                        descripcion = x.cDescripcion,
                        precioBase = x.nPrecioBase,
                        precioVenta = x.nPrecioVenta,
                        codAgencia = x.nCodAge })
                    .Take(1)
                    .Single();

                var tiempoReserva = db.tiemposReservas
                    .Where(x => x.ubicacion == agencia.cDepartamento)
                    .Select( x => new { horas = x.tiempoReserva , dias = x.dias})
                    .Take(1)
                    .Single();

                var dbContextDiagram = new DBECommerceEntities1();

                ObjectParameter salida1 = new ObjectParameter("nResul", typeof(int));
                ObjectParameter salida2 = new ObjectParameter("sFechaSis", typeof(DateTime));

                var resultado = dbContextDiagram
                    .SP_ECommerceRegistrarSeparacion(
                                                moldeSeparacion.cSKU,
                                                moldeSeparacion.dni,
                                                articulo.codAgencia,
                                                salida1,
                                                salida2,
                                                articulo.precioVenta,
                                                moldeSeparacion.telefono,
                                                moldeSeparacion.correo,
                                                tiempoReserva.dias);

                if ( (int)salida1.Value == 0 )
                {
                    return Ok( new { status = 0, mensaje =" Error al realizar la reserva."} );
                } 

                // almacenando la separacion del articulo en la tabla : "articulo_separado"

                articulo_separado regSeparacion = new articulo_separado();
                regSeparacion.cSKU = moldeSeparacion.cSKU;
                regSeparacion.nombre = articulo.nombre;
                regSeparacion.descripcion = articulo.descripcion;
                regSeparacion.precioBase = articulo.precioBase;
                regSeparacion.precioVenta = articulo.precioVenta;
                regSeparacion.fecha = DateTime.Now;
                regSeparacion.codAgencia = articulo.codAgencia;
                regSeparacion.dni = moldeSeparacion.dni;
                regSeparacion.telefono = moldeSeparacion.telefono;
                regSeparacion.correo = moldeSeparacion.correo;
                regSeparacion.fechaRecojo = salida2.Value.ToString();
                db.articulo_separado.Add(regSeparacion);

                
                
                try
                {
                    db.Database.ExecuteSqlCommand(" UPDATE [PER].[reflejoBaseVenta] SET estado = 0 WHERE [cSKU] ='" + moldeSeparacion.cSKU + "'");
                
                    db.SaveChanges();

                }

                catch (Exception e )
                {
                    return Ok(e);
                    
                }

                var cliente = db.VW_ECommerceClientes
                    .Where(x => x.cDNI == moldeSeparacion.dni )
                    .Select(x => x)
                    .Take(1)
                    .Single();
               
                var respuesta = new
                {
                    status = 1,
                    mensaje = "Reserva exitósa",
                    codigoReserva = moldeSeparacion.cSKU,
                    nombre = cliente.cNombres,
                    agencia = agencia,
                    tiempoReserva = tiempoReserva.horas,
                    diasReserva = salida2.Value
                };
                 
              return Ok(respuesta);

            }
            else
            {
                var errorValidacion = new { status = 0, mensaje = "Porfavor inicie sesión." };

                return Ok(errorValidacion);
            }

              
        }

      /*  [Route("api/xd")]
        [HttpGet]
        public IHttpActionResult xdxd()
        {   

            var dbContextDiagram = new DBECommerceEntities1();

            ObjectParameter salida1 = new ObjectParameter("nResul", typeof(int));
            ObjectParameter sFechaSis = new ObjectParameter("sFechaSis", typeof(string));

            var resultado = dbContextDiagram
                .SP_ECommerceRegistrarSeparacion(
                                           "C035000000537904",
                                           "46801098",
                                            84,
                                            salida1,
                                            sFechaSis,
                                            20,
                                            "987532420",
                                            "jose@gmail.com",
                                            3);

            return Ok( new { numero = salida1.Value , fecha = sFechaSis.Value.ToString() } );
        } */

/*
        [Route("api/correo/")]
        [HttpPost]
        public IHttpActionResult EnviarCorreo( MoldeEnvioEmail data )
        {
            if ( CodTokenValidator.ValidarToken(data.token ) == 1)
            {
                using (MailMessage mm = new MailMessage(ConfigurationManager.AppSettings["SMTPuser"], data.correo))
                {
                    mm.Subject = "Informacion de reserva | Inversiones La Cruz";
                    mm.Body = CreateBody(data.nombre, data.sku, data.codAgencia);
                    mm.IsBodyHtml = true;
                    SmtpClient smtp = new SmtpClient();
                    smtp.Host = ConfigurationManager.AppSettings["Host"];
                    smtp.EnableSsl = true;

                    NetworkCredential NetworkCred = new NetworkCredential(ConfigurationManager.AppSettings["SMTPuser"], ConfigurationManager.AppSettings["SMTPpassword"]);
                    smtp.UseDefaultCredentials = Convert.ToBoolean(ConfigurationManager.AppSettings["EnableSSL"]);
                    smtp.Credentials = NetworkCred;
                    smtp.Port = int.Parse(ConfigurationManager.AppSettings["Port"]);
                    smtp.Send(mm);

                }
                return Ok();

            } else
            {
                var errorValidacion = new { status = 0, mensaje = "Porfavor inicie sesión." };

                return Ok(errorValidacion);
            }
            
        }
        */
        private string CreateBody( string nombre, string sku, int codAgencia )
        {
            string body = string.Empty;

            using (StreamReader reader = new StreamReader( HttpContext.Current.Server.MapPath("~/views/mailing/mailing.html")))
            {

                body = reader.ReadToEnd();

            }
           
            var agencia = db.VW_ECommerceAgencias.Where(x => x.nCodAge == codAgencia).Select(x => x).Distinct().Take(1).Single();

            body = body.Replace("{name}", nombre); 
            body = body.Replace("{sku}",sku);
            body = body.Replace("{direccion}", agencia.cDireccion );
            body = body.Replace("{horarioLV}", agencia.cHorarioLV );
            body = body.Replace("{horarioSabado}", agencia.cHorarioS);

            return body;

        }


        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool usuarioExists(int id)
        {
            return db.usuarios.Count(e => e.idUsuario == id) > 0;
        }
    }
}