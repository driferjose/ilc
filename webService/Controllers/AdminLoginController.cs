﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using webService.Models;
using webService.libs;
using System.Data.Entity.Infrastructure;
using System.Security.Cryptography;
using System.Text;

namespace webService.Controllers
{
    public class AdminLoginController : ApiController
    {
        private DBECommerceContext db = new DBECommerceContext();
        private Token CodTokenValidator = new Token();

        [Route("admin/login/")]
        [HttpPost]
        public IHttpActionResult IniciarSesion( AdminMoldeLogueo datosLogueo )
        {
            var validarPass = CodificarPassword(datosLogueo.password);
            var validacion = db.admin_usuario.Where( x => x.usuario == datosLogueo.usuario && x.password == validarPass).Select(x=> x);
                
            if(validacion.Count() > 0)
            {
                admin_sesion nuevaSesion = new admin_sesion();
                nuevaSesion.idUsuario = validacion.Select(x => x.idAdminUsuario).Single();
                nuevaSesion.fechaSesion = DateTime.Now;
                nuevaSesion.token = CodTokenValidator.GenerarTokenAdmin();

                db.admin_sesion.Add(nuevaSesion);
                try
                {
                    db.SaveChanges();
                }
                catch (DbUpdateException)
                {

                    throw;

                }

                var succesful = new { status = 1, mensaje = "Inicio de sesion Exitoso.", token = nuevaSesion.token };

                return Ok(succesful);
            } else
            {
                var error = new { status = 0, mensaje = "Porfavor verifique sus credenciales."};
                return Ok(validarPass);
            }
            

        }

        [Route("admin/registrar/")]
        [HttpGet]
        public IHttpActionResult newAadmin(string usuario, string password, string token )
        {
            var validarToken = db.Database.SqlQuery<int>("SELECT COUNT(*) AS total FROM [PER].[admin_token] WHERE token = '" + token+"'").Single();

            if( validarToken == 1)
            {
                admin_usuario user = new admin_usuario();
                user.usuario = usuario;
                user.password = CodificarPassword(password);
                db.admin_usuario.Add(user);

                db.SaveChanges();

                return Ok("Registro Exitóso.");

            }else
            {
                return Ok("Fallo en las credenciales.");
            }
        }

        [Route("admin/reset-password/")]
        [HttpGet]
        public IHttpActionResult resetPassAdmin(string usuario, string newpassword, string token)
        {
            var validarToken = db.Database.SqlQuery<int>("SELECT COUNT(*) AS total FROM [PER].[admin_token] WHERE token = '" + token + "'").Single();

            if(validarToken == 1)
            {   
                db.Database.ExecuteSqlCommand(" UPDATE admin_usuario SET password = '" + CodificarPassword(newpassword) + "' WHERE usuario ='" + usuario + "' ");
                return Ok("Actualización Exitósa.");
            }
            else
            {
                return Ok("Fallo en las credenciales.");
            }
        }

        [Route("admin/logout/{token}")]
        [HttpGet]
        public IHttpActionResult CerrarSesion( string token )
        {   

            var exito = new { status = 1 };
            try
            {   
                var sesion = db.Database.ExecuteSqlCommand("UPDATE [PER].[admin_sesion] SET [fechaCierreSesion] = '"+ DateTime.Now.ToString("yyyy-MM-dd") + "' WHERE [token] = '"+ token + "' ");
                
                return Ok(exito);
            }
            catch (Exception e)
            {

                return Ok(e);
            }
           
        }
       
        public string CodificarPassword( string word )
        {
            MD5 md5 = new MD5CryptoServiceProvider();

            //compute hash from the bytes of text
            md5.ComputeHash(ASCIIEncoding.ASCII.GetBytes(word));

            //get hash result after compute it
            byte[] result = md5.Hash;

            StringBuilder strBuilder = new StringBuilder();
            for (int i = 0; i < result.Length; i++)
            {
                //change it into 2 hexadecimal digits
                //for each byte
                strBuilder.Append(result[i].ToString("x2"));
            }

            return strBuilder.ToString();
        }

    }
}
