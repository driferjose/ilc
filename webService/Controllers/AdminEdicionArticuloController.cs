﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Data.Entity.Infrastructure;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web;
using System.Web.Hosting;
using System.Web.Http;
using webService.libs;
using webService.Models;

namespace webService.Controllers
{
    public class AdminEdicionArticuloController : ApiController
    {

        private DBECommerceContext db = new DBECommerceContext();
        private Token CodTokenValidator = new Token();

        [Route("admin/articulo/subir-imagen")]
        [HttpPost]
        public IHttpActionResult EditarArticulo( )
        {
            var context = HttpContext.Current.Request;
            if (CodTokenValidator.ValidarTokenAdmin( context.Form["token"]) == 1)
            {
                Imagen img = new Imagen();

                var skuRecibido = context.Form["sku"];

                if (context.Files.Count > 0)
                {
                    List<articuloImagen> tempReg = new List<articuloImagen>();
                    List<int> idsImgs = new List<int>();

                    for (var i = 0; i < context.Files.Count; i++)
                    {

                        var binaryReader = new BinaryReader(context.Files[i].InputStream);
                        var fileData = binaryReader.ReadBytes(context.Files[i].ContentLength);


                        MemoryStream ms = new MemoryStream();

                        ms.Write(fileData, 0, fileData.Length);

                        Image imgRecibida = Image.FromStream(ms, true);

                        articuloImagen registro = new articuloImagen();

                        registro.imgSizeSmall = img.GuardarImagen(imgRecibida, 100, skuRecibido);
                        registro.imgSizeMediun = img.GuardarImagen(imgRecibida, 800, skuRecibido);
                        registro.imgSizeOriginal = img.GuardarImagen(imgRecibida, 0, skuRecibido);
                        registro.sku = skuRecibido;
                        registro.activo = true;
                        registro.predeterminado = false;

                        db.articuloImagens.Add(registro);
                        tempReg.Add(registro);
                    }

                    try
                    {
                        db.SaveChanges();
                        tempReg.ToArray();
                        foreach (var objeto in tempReg)
                        {
                            idsImgs.Add(objeto.id);
                        }
                        idsImgs.ToArray();
                    }
                    catch (DbUpdateException)
                    {
                        throw;
                    }

                    var exito = new { status = 1, mensaje = "Subida exitosa.", ids = idsImgs };

                    return Ok(exito);
                }
                else
                {
                    var falla = new { status = 0, mensaje = "No se han subido imagenes." };

                    return Ok(falla);
                }

            } else
            {
                var errorLogueo = new { status = 0, mensaje = "Porfavor inicie sesión." };
                return Ok(errorLogueo);

            }
               

        }

        [Route("admin/articulo/editar/")]
        [HttpPost]
        public IHttpActionResult EditarInformacion( AdminEditarArticulo datosEditar )
        {
            if (CodTokenValidator.ValidarTokenAdmin(datosEditar.token) == 1)
            {
                var idAdmin = db.admin_sesion.Where(x => x.token == datosEditar.token).Select( x => x.idUsuario).Take(1).SingleOrDefault();
                articuloEdicion edicion = new  articuloEdicion();
                edicion.idAdminSesion = idAdmin;
                edicion.articuloEditado = datosEditar.sku;
                edicion.fechaEdicion = DateTime.Now;
                db.articuloEdicions.Add(edicion);

                try
                {
                    db.SaveChanges();
                }
                catch (Exception)
                {

                    throw;
                }

                if ( datosEditar.imgElimanadas != null && datosEditar.imgElimanadas.Length > 0 )
                {
                    foreach (int id in datosEditar.imgElimanadas)
                    {
                        db.Database.ExecuteSqlCommand(" UPDATE [PER].[articuloImagen] SET [activo] = 0 WHERE [id] = " + id + ";");
                    }
                }

                try
                {

                    db.Database.ExecuteSqlCommand(" UPDATE [PER].[reflejoBaseVenta] " +
                                                  " SET [cArticulo] = '" + datosEditar.nombreArticulo +
                                                  "', [nPrecioBase] = " + datosEditar.precioNormal +
                                                  ", [cDescripcion] =  '" +datosEditar.descripcion+ "' "+
                                                  ", [cObservaciones] =  '" + datosEditar.observaciones + "' " +
                                                  ", [oferta] = '" + datosEditar.oferta +"' "+
                                                  "  WHERE [cSKU] = '" + datosEditar.sku + "'; ");

                    db.Database.ExecuteSqlCommand("[PER].[SP_actualizarPrecio] '"+ datosEditar.sku + "' , "+ datosEditar.precioVenta + " ");
                }

                catch (Exception e)
                {
                    var error = new { status = 0, mensaje = "Hubo un error al actualizar la informacion" };
                    return Ok(error);
                }

                var exito = new { status = 1, mensaje = "Actualizacion exitosa." };

                return Ok(exito);
            }
            else
            {
                var errorLogueo = new { status = 0, mensaje = "Porfavor inicie sesión." };
                return Ok(errorLogueo);
            }
               
        }

        [Route("admin/elimina/articulo")]
        [HttpPost]
        public IHttpActionResult EliminarArticulo( MoldeEliminarArticulo articulo )
        {
            if (CodTokenValidator.ValidarTokenAdmin(articulo.token) == 1)
            {
                var exito = new { status = 1, mensaje = "Eliminado exitosamente" };
                var error = new { status = 0, mensaje = "Error al eliminar" };
                try
                {
                    db.Database.ExecuteSqlCommand("UPDATE [PER].[reflejoBaseVenta] SET [estado] = 3 WHERE [cSKU] = '" + articulo.sku + "';");

                    return Ok(exito);
                }
                catch (Exception e)
                {

                    return Ok(error);

                }
            } else
            {
                var errorLogueo = new { status = 0, mensaje = "Porfavor inicie sesión." };
                return Ok(errorLogueo);
            }
            
        }

        [Route("admin/activar/articulo")]
        [HttpPost]
        public IHttpActionResult activarArticulo(MoldeEliminarArticulo articulo)
        {
            if (CodTokenValidator.ValidarTokenAdmin(articulo.token) == 1)
            {
                var exito = new { status = 1, mensaje = "activado exitosamente" };
                var error = new { status = 0, mensaje = "Error al activar" };
                try
                {
                    db.Database.ExecuteSqlCommand("UPDATE [PER].[reflejoBaseVenta] SET [estado] = 1 WHERE [cSKU] = '" + articulo.sku + "';");

                    return Ok(exito);
                }
                catch (Exception e)
                {

                    return Ok(error);

                }
            }
            else
            {
                var errorLogueo = new { status = 0, mensaje = "Porfavor inicie sesión." };
                return Ok(errorLogueo);
            }

        }

        [Route("admin/imagen/predeterminada")]
        [HttpPost]
        public IHttpActionResult EstablecerImagenPredeterminada(AdminRecibirImgPred imgPredDatos )
        {
            if (CodTokenValidator.ValidarTokenAdmin(imgPredDatos.token) == 1)
            {
                var exito = new { status = 1, mensaje = "Actualizacion exitosa." };
                var error = new { status = 0, mensaje = "Error al actualizar." };

                try
                {
                    db.Database.ExecuteSqlCommand("UPDATE [PER].[articuloImagen] SET [predeterminado] = 0 WHERE [sku] = '" + imgPredDatos.sku + "'");
                    db.Database.ExecuteSqlCommand("UPDATE [PER].[articuloImagen] SET [predeterminado] = 1 WHERE [id] = " + imgPredDatos.idImagen + "");

                    return Ok(exito);
                }
                catch (Exception)
                {
                    return Ok(error);
                }
                
            } else
            {
                var errorLogueo = new { status = 0, mensaje = "Porfavor inicie sesión." };
                return Ok(errorLogueo);
            }
        }
    }
}
