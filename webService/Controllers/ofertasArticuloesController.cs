﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using webService.libs;
using webService.Models;

namespace webService.Controllers
{
    public class ofertasArticuloesController : ApiController
    {
        private DBECommerceContext db = new DBECommerceContext();
        private Token CodTokenValidator = new Token();

        [Route("api/ofertas/")]
        public IHttpActionResult PostofertasArticulo( FiltroOfertas peticion )
        {
            if (CodTokenValidator.ValidarToken(peticion.token) == 1 )
            {
                String filtro = String.Empty;

               if( peticion.rango != 0)
                {
                    var rango = db.rangoPrecios.Where(x => x.nIdRango == peticion.rango).Select(x => new
                    {
                        varlorMin = x.cValorMinimo,
                        varlorMax = x.cValorMaximo

                    }).Take(1);

                    int? valorMin;
                    int? valorMax;

                    if (rango.Count() == 0)
                    {
                        valorMin = null;
                        valorMax = null;

                    }
                    else
                    {
                        valorMin = rango.Single().varlorMin;
                        valorMax = rango.Single().varlorMax;
                        filtro = "AND a.nPrecioVenta > "+ valorMin + " AND a.nPrecioVenta < "+valorMax+"  ";
                    }
                }

                var productosBaseVenta = db.Database
                    .SqlQuery<FiltroProducto>(" SELECT A.[cSKU] AS sku , MIN(A.[cArticulo]) AS nombre , MIN(B.imgSizeSmall) AS imagen , MIN(A.[nPrecioBase]) AS precioNormal, " +
                                                " MIN(A.[nPrecioVenta]) AS precioVenta " +
                                                " FROM [PER].[reflejoBaseVenta] A " +
                                                " LEFT JOIN [PER].[articuloImagen] B ON [cSKU] = [sku] " +
                                                " WHERE [cSKU] IS NOT NULL AND B.imgSizeSmall IS NOT NULL  AND  A.[oferta] = 1 AND A.[estado] = 1 "+filtro+" AND  ( B.[predeterminado] IS NULL OR B.[predeterminado] = 1 ) " +
                                                " GROUP BY A.[cSKU] ORDER BY precioVenta ");

                if (productosBaseVenta.Count() == 0 )
                {
                    return Ok( new { status = 1 , mensaje = "No hay ofertas disponibles " } );
                }
                return Ok(productosBaseVenta);
           

            } else
            {
                var errorValidacion = new { status = 0, mensaje = "Porfavor inicie sesión." };

                return Ok(errorValidacion);
            }
            
        }
    }
}