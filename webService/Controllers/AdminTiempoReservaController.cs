﻿using OfficeOpenXml.FormulaParsing.LexicalAnalysis;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using webService.Models;
using webService.libs;

namespace webService.Controllers
{
    public class AdminTiempoReservaController : ApiController
    {
        private DBECommerceContext db = new DBECommerceContext();
        private libs.Token CodTokenValidator = new libs.Token();


        [Route("admin/editar/tiempo-reserva")]
        [HttpPost]

        public IHttpActionResult ActualizarTiempoReserva(TiempoReservaEditar editar)
        {
            try
            {
                if ( editar.departamento.ToUpper() == "LIMA")
                {
                    
                    db.Database.ExecuteSqlCommand(" UPDATE [PER].[tiemposReserva] SET [dias]  = " + editar.dias + " WHERE [ubicacion] = 'LIMA'  ;");

                } else if (editar.departamento.ToUpper() == "PROVINCIA" )
                {
                    db.Database.ExecuteSqlCommand(" UPDATE [PER].[tiemposReserva] SET [dias]  = " + editar.dias + " WHERE [ubicacion] != 'LIMA'  ;");
                }

                
            }
            catch (Exception)
            {
                var error = new { status = 0, mensaje = "Hubo un error al actualizar la información" };
                return Ok(error);
            }
            var exito = new { status = 1, mensaje = "Actualizado correctamente" };
            return Ok(exito);
        }

        [Route("admin/tiempo-reserva/{token}")]
        [HttpGet]

        public IHttpActionResult ObtenerTiemposDeReserva( string token )
        {
            try
            {
                var tr = db.tiemposReservas
                    .Where( x => x.ubicacion == "LIMA" )
                    .Select(x => new { departamento = x.ubicacion, dias = x.dias }).Take(1).Single();
                var provTR = db.tiemposReservas
                    .Where(x => x.ubicacion != "LIMA").Take(1).Select( x => new { departamento = "PROVINCIA", dias = x.dias }).Take(1).Single();

                var lima = new AdminMoldeTiemposDeReserva(tr.departamento, tr.dias );
                var prov = new AdminMoldeTiemposDeReserva(provTR.departamento, provTR.dias);
                List<AdminMoldeTiemposDeReserva> trFinal = new List<AdminMoldeTiemposDeReserva>();
                trFinal.Add(lima);
                trFinal.Add(prov);
                trFinal.ToArray();

                return Ok(trFinal);
            }

            catch (Exception)
            {
                var error = new { status = 0, mensaje = "Hubo un error al obtener la informacion" };
                return Ok(error);
            }
            
        }
    }
}
