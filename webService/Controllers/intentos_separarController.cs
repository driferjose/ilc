﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using webService.libs;
using webService.Models;

namespace webService.Controllers
{
    public class intentos_separarController : ApiController
    {
        private DBECommerceContext db = new DBECommerceContext();
        private Token CodTokenValidator = new Token();

        [Route("api/intento_separar/{sku}/{token}")]
        [HttpGet]
        public IHttpActionResult IntentoSepararArticulo(string sku, string token )
        {
            if(CodTokenValidator.ValidarToken(token) == 1)
            {
                var Idsesion = db.sesions.Where(x => x.token == token).Select(x => x.idSesion).Take(1).Single();
                var idArticulo = db.reflejoBaseVentas.Where(x => x.cSKU == sku).Select(x => x.nIdArticulo ).Take(1).Single();


                intentos_separar intento = new intentos_separar();
                intento.idSesion = Idsesion;
                intento.fecha = DateTime.Now;
                intento.idArticulo = idArticulo;

                if (!ModelState.IsValid)
                {
                    return BadRequest(ModelState);
                }

                db.intentos_separar.Add(intento);

                try
                {
                    db.SaveChanges();
                }
                catch (DbUpdateException)
                {

                    throw;

                }

                var exito = new { status = 1 };

                return Ok(exito);
            }
            else
            {
                var errorValidacion = new { status = 0, mensaje = "Porfavor inicie sesión." };

                return Ok(errorValidacion);
            }
            
        }
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool intentos_separarExists(int id)
        {
            return db.intentos_separar.Count(e => e.idIntento == id) > 0;
        }
    }
}