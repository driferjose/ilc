﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using webService.libs;
using webService.Models;

namespace webService.Controllers
{
    public class reflejoBaseVentasController : ApiController
    {
        private DBECommerceContext db = new DBECommerceContext();
        private Token CodTokenValidator = new Token();

        [Route("api/busqueda/")]
        [HttpPost]
        public IHttpActionResult BuscarProductos(Busqueda palabra)
        {
            if ( CodTokenValidator.ValidarToken(palabra.token) == 1)
            {
                var palabraParaSQL = String.Empty;
                var contadorQuery = 0;

                List<string> palabrasArray = palabra
                    .palabra
                    .Split( new string[] { " ", "  ", "   " }, StringSplitOptions.None)
                    .ToList();

                palabrasArray.RemoveAll(str => String.IsNullOrEmpty(str));

                foreach (var palabrita in palabrasArray)
                {   
                    palabraParaSQL += " A.[cArticulo] LIKE '%" + palabrita +
                                        "%' OR A.[cDescripcion] LIKE '%"+ palabrita + "%' "+
                                        "OR A.[cMarca] LIKE '%"+ palabrita +
                                        "%' OR A.[cFamilia] LIKE '%"+ palabrita + "%' ";
                    contadorQuery++;

                    if(contadorQuery < palabrasArray.Count)
                    {
                        palabraParaSQL += " OR ";
                    }
                }

                var busqueda = db.Database
                    .SqlQuery<ResultadoBusqueda>(" SELECT TOP 10 A.[cSKU] AS sku ,"+
                                                "  MIN(A.[cArticulo]) AS nombre ,"+
                                                "  MIN(B.imgSizeSmall) AS imagen ,"+
                                                "  MIN(A.[nPrecioBase]) AS precioNormal, " +
                                                "  MIN(A.[nPrecioVenta] ) precioVenta, " +
                                                "  MIN(A.[cDescripcion]) AS descripcion, "+
                                                "  MIN(A.[cMarca]) AS marca, "+
                                                "  MIN(A.[cFamilia]) AS familia " +
                                                "  FROM [PER].[reflejoBaseVenta] A " +
                                                "  JOIN [PER].[articuloImagen] B ON [cSKU] = [sku] " +
                                                "  WHERE A.estado = 1  AND  ( " +  palabraParaSQL + " ) "+
                                                "  GROUP BY A.[cSKU] ORDER BY precioVenta  ");

                List<OrdenResultadosBusqueda> resultadoFinal = new List<OrdenResultadosBusqueda>();

                foreach (var row in busqueda)
                {
                    var contadorCoincidencias = 0;

                    foreach (var palabrita in palabrasArray)
                    {
                        if(row.nombre.IndexOf(palabrita.ToUpper()) > 1)
                        {
                            contadorCoincidencias++;
                        }
                        if (row.descripcion.IndexOf(palabrita.ToUpper()) > 1)
                        {
                            contadorCoincidencias++;
                        }
                        if (row.marca.IndexOf(palabrita.ToUpper()) > 1)
                        {
                            contadorCoincidencias++;
                        }
                        if (row.familia.IndexOf(palabrita.ToUpper()) > 1)
                        {
                            contadorCoincidencias++;
                        }

                    }
                    OrdenResultadosBusqueda result = new OrdenResultadosBusqueda() ;
                    result.fila = row;
                    result.coincidencias = contadorCoincidencias;
                    resultadoFinal.Add(result);
                }
               
                resultadoFinal.ToArray();
                
                if (resultadoFinal.Count() > 0)
                {
                    return Ok(resultadoFinal.OrderByDescending(x => x.coincidencias).Select(x => x.fila));

                }
                else
                {
                    var noFound = new { status = 0, mensaje = "No hay resultados" };
                    return Ok(noFound);
                }
            } else
            {
                var errorValidacion = new { status = 0, mensaje = "Porfavor inicie sesión." };

                return Ok(errorValidacion);
            }
            
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

       
    }
}