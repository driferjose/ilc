﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using webService.Models;

namespace webService.Controllers
{
    public class AdminAgenciasController : ApiController
    {
        private DBECommerceContext db = new DBECommerceContext();
        private libs.Token CodTokenValidator = new libs.Token();

        [Route("admin/agencias/{token}")]
        [HttpGet]
        public IHttpActionResult ObtenerAgencias(string token )
        {   
            if(CodTokenValidator.ValidarTokenAdmin(token) == 1 )
            {
                var agencias = db.Database.SqlQuery<AdminEnvioAgencias>(" SELECT DISTINCT A.nCodAge AS codAge, A.cAgencia AS nombre FROM  " +
                                                                        " [PER].[VW_ECommerceAgencias] A " +
                                                                        " JOIN [PER].[reflejoBaseVenta] B ON A.nCodAge = B.[nCodAge] ORDER BY A.cAgencia ");
                return Ok(agencias);

            } else
            {   
                var errorLogueo = new { status = 0, mensaje = "Porfavor inicie sesión." };
                return Ok(errorLogueo);
            }

        }
    }
}
