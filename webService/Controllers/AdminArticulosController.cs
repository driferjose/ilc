﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using webService.libs;
using webService.Models;

namespace webService.Controllers
{
    public class AdminArticulosController : ApiController
    {
        private DBECommerceContext db = new DBECommerceContext();
        private Token CodTokenValidator = new Token();

        [Route("admin/articulos/")]
        [HttpPost]
        public IHttpActionResult FiltrarArticulos( AdminFiltroArticulos dataRecibida )
        {
            if (CodTokenValidator.ValidarTokenAdmin(dataRecibida.token) == 1 )
            {
                string filtros = String.Empty;

                if (dataRecibida.oferta == true)
                {
                    filtros += "AND [oferta] = 1 ";
                }

                if (dataRecibida.sku != "0")
                {
                    filtros += " AND [cSKU] LIKE '%" + dataRecibida.sku + "%' ";
                    
                }

                if (dataRecibida.editado == true)
                {
                    filtros += " AND  A.[cSKU] =  B.[articuloEditado] ";
                    
                }

                if (dataRecibida.agencia != 0 )
                {
                    filtros += "AND A.[nCodAge] = " + dataRecibida.agencia + "  ";
                }

                if (dataRecibida.eliminado == true )
                {
                    filtros += " AND A.estado = 3 ";

                }else if (dataRecibida.eliminado == false )
                {
                    filtros += " AND A.estado = 1 ";
                }

                var consulta = db.Database.
                    SqlQuery<AdminRespuestaFiltroArticulo>(" SELECT DISTINCT A.[cArticulo] AS nombreArticulo , C.[cAgencia] AS agencia, A.[cSKU] AS sku "+
                                                            "  FROM [PER].[reflejoBaseVenta] A " +
                                                           " LEFT JOIN [PER].[articuloEdicion] B ON A.[cSKU] = B.[articuloEditado] " +
                                                           " LEFT JOIN [PER].[VW_ECommerceAgencias] C ON A.[nCodAge] = C.[nCodAge] " +
                                                           " WHERE  A.[cSKU] IS NOT NULL  " + filtros + " ORDER BY  A.[cArticulo] ");
                
                return Ok(consulta);
            }
            else
            {
                var errorLogueo = new { status = 0, mensaje = "Porfavor inicie sesión." };
                return Ok(errorLogueo);
            }
            
        }


        [Route("admin/articulo/{token}/{sku}")]
        [HttpGet]
        public IHttpActionResult MostrarArticulo(string token, string sku)
        {
            if (CodTokenValidator.ValidarTokenAdmin(token) == 1)
            {
                var imgs = db.articuloImagens
                    .Where( x => x.sku == sku && x.activo == true )
                    .Select(x => new { idImg = x.id, imgSmall = x.imgSizeSmall, imgMediano = x.imgSizeMediun });

                var idImgPredeterminada = db.articuloImagens
                    .Where(x => x.sku == sku && x.activo == true && x.predeterminado == true )
                    .Select( x => x.id )
                    .Take(1).SingleOrDefault();

                var detalle = db.reflejoBaseVentas
                    .Where(x => x.cSKU == sku)
                    .Join(db.VW_ECommerceAgencias, a => a.nCodAge, b => b.nCodAge, (venta, agencia) => new
                    {
                        idArticulo = venta.nIdArticulo,
                        codArticulo = venta.nCodArticulo,
                        nombreProducto = venta.cArticulo,
                        agenciaDirecion = agencia.cDireccion,
                        nombreAgencia = agencia.cAgencia,
                        imagenes = imgs,
                        marca = venta.cMarca,
                        precioNormal = venta.nPrecioBase,
                        precioVenta = venta.nPrecioVenta,
                        ubicacion = agencia.cDepartamento,
                        familia = venta.cFamilia,
                        descripcion = venta.cDescripcion,
                        observaciones = venta.cObservaciones,
                        oferta = venta.oferta,
                        imgPredeterminado = idImgPredeterminada
                    });

                if (detalle.Count() == 0)
                {
                    var error = new { status = 0, mensaje = "No se ha encontrado el artículo" };
                    return Ok(error);
                }

                return Ok(detalle);

            } else
            {
                var errorLogueo = new { status = 0, mensaje = "Porfavor inicie sesión." };
                return Ok(errorLogueo);
            }
               
        }

    }
}
