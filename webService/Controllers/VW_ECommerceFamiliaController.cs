﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using webService.libs;
using webService.Models;


namespace webService.Controllers
{
    public class VW_ECommerceFamiliaController : ApiController
    {
        private DBECommerceContext db = new DBECommerceContext();
        private Token CodTokenValidator = new Token();

        [Route("api/init/{codToken}")]
        [HttpGet]
        public IHttpActionResult GetInicializacion( string codToken )
        {
            if ( CodTokenValidator.ValidarToken(codToken) == 1 )
            {
                var arrayMarcas = db.reflejoBaseVentas
                    .Where( x => x.estado == 1 )
                    .Select(x => new
                    {
                        codFamilia = x.nCodFamilia,
                        nombreMarca = x.cMarca

                    }).Distinct().ToList();

                var ArrayFamilia = db.Database
                    .SqlQuery<ResultadoInit>(" SELECT A.[nCodFamilia] AS codigo, MIN(A.[cFamilia]) AS nombre, MIN(B.iconoUrl) AS imgUrl, MAX(A.[nPrecioVenta]) AS precioMax  " +
                                            " FROM [PER].[reflejoBaseVenta] A "+
                                            " LEFT JOIN [PER].[reflejoFamilia] B ON A.[nCodFamilia] = B.[nCodFamilia] "+
                                            " RIGHT JOIN  [PER].[articuloImagen] AS C ON A.cSKU = C.sku "+
                                            " WHERE A.estado = 1 AND  A.[cFamilia] != '' AND C.sku IS NOT NULL "+
                                            "  GROUP BY A.[nCodFamilia]   ORDER BY MIN(A.[cFamilia]) ").ToList();

                var listFiltrs = db.Database
                                .SqlQuery<MoldeFiltroFamilias>("SELECT  MIN(A.[nCodAge]) AS codAgencia, MIN(A.[nCodFamilia]) AS codFamilia , MIN(B.[cAgencia]) AS nombre "+
                                                            " FROM [PER].[reflejoBaseVenta] A  " +
                                                            "JOIN [PER].[VW_ECommerceAgencias] B ON A.[nCodAge] = B.[nCodAge] "+
                                                            " WHERE A.estado = 1 " +
                                                           " GROUP BY A.[nCodAge], A.[nCodFamilia] ").ToList();
                

               

                var rangoPrecios = db.rangoPrecios.Where(x => x.cEstado == true)
                                .Select(x => new { idRangoPrecio = x.nIdRango, nombreRango = x.cNombreRango } )
                                .ToList();

                List<RangoInit> listaRangos = new List<RangoInit>();
                foreach (var item in rangoPrecios)
                {
                    RangoInit rang = new RangoInit();
                    rang.idRangoPrecio = item.idRangoPrecio;
                    rang.nombreRango = item.nombreRango;
                    listaRangos.Add(rang);
                }

                List<FamiliaModelo> arrayFamilias = new List<FamiliaModelo>();

                foreach (var datos in ArrayFamilia)
                {
                    List<string> termsList = new List<string>();

                    foreach ( var relacion in arrayMarcas )
                    {
                        if (datos.codigo == relacion.codFamilia)
                        {
                            termsList.Add(relacion.nombreMarca);
                        }
                    }
                    
                    var familia = new FamiliaModelo(datos.nombre, datos.codigo, datos.imgUrl, termsList.ToArray(), null);

                    arrayFamilias.Add(familia);
                }


                foreach (var fam in arrayFamilias)
                {
                    List<ResultadoFamilias> codAge = new List<ResultadoFamilias>();
                    foreach (var row in listFiltrs)
                    {
                         ResultadoFamilias tempFam = new ResultadoFamilias();
                        if ( fam.Codigo == row.codFamilia )
                        {
                            tempFam.codAgencia = row.codAgencia;
                            tempFam.nombre = row.nombre;
                            codAge.Add(tempFam);
                        }
                    }
                    fam.CodUbicacion = codAge.ToList();
                }
                
                var jsonSalida = new InicioOut();

                jsonSalida.ArrayFamilias = arrayFamilias;
                jsonSalida.RangoPrecios = listaRangos;

                return Ok(jsonSalida);

            } else
            {
                var errorValidacion = new { status = 0, mensaje = "Porfavor inicie sesión." };

                return Ok(errorValidacion);
            }
            

        }
        
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool VW_ECommerceFamiliaExists(int id)
        {
            return db.VW_ECommerceFamilia.Count(e => e.nCodFamilia == id) > 0;
        }
    }
}