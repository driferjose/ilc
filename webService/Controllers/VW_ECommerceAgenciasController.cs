﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using webService.libs;
using webService.Models;

namespace webService.Controllers
{
    public class VW_ECommerceAgenciasController : ApiController
    {
        private DBECommerceContext db = new DBECommerceContext();
        private Token CodTokenValidator = new Token();

        [Route("api/agencias")]
        [HttpGet]
        public IHttpActionResult ObtenerAgencias()
        {
            var agencias = db.VW_ECommerceAgencias.Where( x => !x.cAgencia.Contains("ALMACEN"))
                .Select( x => new
                                {
                                    nombre = x.cAgencia,
                                    codAgencia = x.nCodAge,
                                    distrito = x.cDitrito
                                } ).Distinct().OrderBy( x => x.nombre );

            if(agencias.Count() == 0)
            {
                var error = new { status = 0, mensaje = "No hay agencias disponibles." }; 
                return Ok(error);
            }

            return Ok(agencias);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool VW_ECommerceAgenciasExists(int id)
        {
            return db.VW_ECommerceAgencias.Count(e => e.nCodAge == id) > 0;
        }
    }
}