﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace webService.Models
{
    public class TiempoReservaEditar
    {
        public string token { get; set; }
        public string departamento { get; set; }
        public int dias { get; set; }
    }
}