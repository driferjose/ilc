﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Web;

namespace webService.Models
{
    public class AdminSubirImg
    {
        public Image[] imagenes { get; set; }
        public string sku { get; set; }
        public int[] idImgsBorradas { get; set; }

    }
}