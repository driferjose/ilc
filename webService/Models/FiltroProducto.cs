﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace webService.Models
{
    public class FiltroProducto
    {

        public string sku { get; set; }
        public string nombre { get; set; }
        public decimal? precioNormal { get; set; }
        public decimal? precioVenta { get; set; }
        public string imagen { get; set; }
    }
}