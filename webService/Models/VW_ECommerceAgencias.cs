namespace webService.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("PER.VW_ECommerceAgencias")]
    public partial class VW_ECommerceAgencias
    {
        [Key]
        [Column(Order = 0)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int nCodAge { get; set; }

        [Key]
        [Column(Order = 1)]
        [StringLength(50)]
        public string cAgencia { get; set; }

        [StringLength(100)]
        public string cDepartamento { get; set; }

        [StringLength(250)]
        public string cProvincia { get; set; }

        [StringLength(250)]
        public string cDitrito { get; set; }

        [StringLength(200)]
        public string cDireccion { get; set; }

        [StringLength(20)]
        public string cTelefono { get; set; }

        [StringLength(100)]
        public string cHorarioLV { get; set; }

        [StringLength(100)]
        public string cHorarioS { get; set; }

        [StringLength(100)]
        public string cHorarioD { get; set; }

        [StringLength(100)]
        public string nLatitud { get; set; }

        [StringLength(100)]
        public string nLongitud { get; set; }
    }
}
