namespace webService.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("PER.reflejoArticulo")]
    public partial class reflejoArticulo
    {
        [Key]
        public int idArticulo { get; set; }

        [StringLength(500)]
        public string cArticulo { get; set; }

        public int? nCodMarca { get; set; }

        [StringLength(50)]
        public string cSKU { get; set; }

        public string cDescripcion { get; set; }

        [Column(TypeName = "money")]
        public decimal? nPrecioBase { get; set; }

        [Column(TypeName = "money")]
        public decimal? nPrecioVenta { get; set; }

        public bool? bActivo { get; set; }

        public bool? oferta { get; set; }
        
    }
}
