namespace webService.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("PER.admin_usuario")]
    public partial class admin_usuario
    {
        [Key]
        public int idAdminUsuario { get; set; }

        [StringLength(50)]
        public string usuario { get; set; }

        [StringLength(250)]
        public string password { get; set; }
    }
}
