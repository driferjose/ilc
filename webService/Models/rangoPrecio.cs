namespace webService.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("PER.rangoPrecios")]
    public partial class rangoPrecio
    {
        [Key]
        public int nIdRango { get; set; }

        [StringLength(50)]
        public string cNombreRango { get; set; }

        public int? cValorMinimo { get; set; }

        public int? cValorMaximo { get; set; }

        public bool cEstado { get; set; }
    }
}
