namespace webService.Models
{
    using System;
    using System.Data.Entity;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;
    using System.Data.Entity.Infrastructure;

    public partial class DBECommerceContext : DbContext
    {
        public DBECommerceContext()
            : base("name=DBECommerceContext")
        {
            ((IObjectContextAdapter)this).ObjectContext.CommandTimeout = 360;
        }

        public virtual DbSet<rangoPrecio> rangoPrecios { get; set; }
        public virtual DbSet<estado> estado { get; set; }
        public virtual DbSet<reflejoFamilia> reflejoFamilias { get; set; }
        public virtual DbSet<VW_ECommerceArticulo> VW_ECommerceArticulo { get; set; }
        public virtual DbSet<VW_ECommerceBaseVenta> VW_ECommerceBaseVenta { get; set; }
        public virtual DbSet<VW_ECommerceFamilia> VW_ECommerceFamilia { get; set; }
        public virtual DbSet<VW_ECommerceLinea> VW_ECommerceLinea { get; set; }
        public virtual DbSet<VW_ECommerceMarca> VW_ECommerceMarca { get; set; }
        public virtual DbSet<VW_ECommerceModelo> VW_ECommerceModelo { get; set; }
        public virtual DbSet<VW_ECommerceAgencias> VW_ECommerceAgencias { get; set; }
        public virtual DbSet<VW_ECommerceClientes> VW_ECommerceClientes { get; set; }
        public virtual DbSet<VW_ECommerceArticuloImagen> VW_ECommerceArticuloImagen { get; set; }
        public virtual DbSet<usuarios> usuarios { get; set; }
        public virtual DbSet<articuloImagen> articuloImagens { get; set; }
        public virtual DbSet<tiemposReserva> tiemposReservas { get; set; }
        public virtual DbSet<reflejoBaseVenta> reflejoBaseVentas { get; set; }
        public virtual DbSet<intentos_separar> intentos_separar { get; set; }
        public virtual DbSet<navegacion> navegacions { get; set; }
        public virtual DbSet<sesion> sesions { get; set; }
        public virtual DbSet<servicioSMS> servicioSMS { get; set; }
        public virtual DbSet<articuloEdicion> articuloEdicions { get; set; }
        public virtual DbSet<articulo_visto> articulo_visto { get; set; }
        public virtual DbSet<articulo_separado> articulo_separado { get; set; }
        public virtual DbSet<indicador> indicadors { get; set; }
        public virtual DbSet<admin_sesion> admin_sesion { get; set; }
        public virtual DbSet<admin_usuario> admin_usuario { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<admin_sesion>()
              .Property(e => e.token)
              .IsUnicode(false);

            modelBuilder.Entity<admin_usuario>()
                .Property(e => e.usuario)
                .IsUnicode(false);

            modelBuilder.Entity<admin_usuario>()
                .Property(e => e.password)
                .IsUnicode(false);

            modelBuilder.Entity<servicioSMS>()
              .Property(e => e.usuario)
              .IsUnicode(false);

            modelBuilder.Entity<indicador>()
                .Property(e => e.descripcion)
                .IsUnicode(false);

            modelBuilder.Entity<articuloEdicion>()
              .Property(e => e.articuloEditado)
              .IsUnicode(false);
            

            modelBuilder.Entity<servicioSMS>()
                .Property(e => e.clave)
                .IsUnicode(false);

            modelBuilder.Entity<servicioSMS>()
                .Property(e => e.url)
                .IsUnicode(false);

            modelBuilder.Entity<rangoPrecio>()
                .Property(e => e.cNombreRango)
                .IsUnicode(false);

            modelBuilder.Entity<tiemposReserva>()
                .Property(e => e.ubicacion)
                .IsUnicode(false);
            modelBuilder.Entity<reflejoBaseVenta>()
               .Property(e => e.cSKU)
               .IsUnicode(false);

            modelBuilder.Entity<reflejoBaseVenta>()
                .Property(e => e.cFamilia)
                .IsUnicode(false);
            modelBuilder.Entity<navegacion>()
              .Property(e => e.cMarca)
              .IsUnicode(false);

            modelBuilder.Entity<sesion>()
                .Property(e => e.token)
                .IsUnicode(false);

            modelBuilder.Entity<sesion>()
                .Property(e => e.usuario)
                .IsUnicode(false);

            modelBuilder.Entity<reflejoBaseVenta>()
                .Property(e => e.cArticulo)
                .IsUnicode(false);

            modelBuilder.Entity<reflejoBaseVenta>()
                .Property(e => e.cMarca)
                .IsUnicode(false);

            modelBuilder.Entity<reflejoBaseVenta>()
                .Property(e => e.cLinea)
                .IsUnicode(false);

            modelBuilder.Entity<reflejoBaseVenta>()
                .Property(e => e.cModelo)
                .IsUnicode(false);

            modelBuilder.Entity<reflejoBaseVenta>()
                .Property(e => e.cDescripcion)
                .IsUnicode(false);

            modelBuilder.Entity<reflejoBaseVenta>()
                .Property(e => e.nPrecioBase)
                .HasPrecision(19, 4);

            modelBuilder.Entity<reflejoBaseVenta>()
                .Property(e => e.nPrecioVenta)
                .HasPrecision(19, 4);

            modelBuilder.Entity<reflejoFamilia>()
                .Property(e => e.iconoUrl)
                .IsUnicode(false);

            modelBuilder.Entity<VW_ECommerceArticulo>()
                .Property(e => e.cDescripcion)
                .IsUnicode(false);

            modelBuilder.Entity<VW_ECommerceBaseVenta>()
                .Property(e => e.cSKU)
                .IsUnicode(false);

            modelBuilder.Entity<VW_ECommerceBaseVenta>()
                .Property(e => e.cFamilia)
                .IsUnicode(false);

            modelBuilder.Entity<VW_ECommerceBaseVenta>()
                .Property(e => e.cArticulo)
                .IsUnicode(false);

            modelBuilder.Entity<VW_ECommerceBaseVenta>()
                .Property(e => e.cMarca)
                .IsUnicode(false);

            modelBuilder.Entity<VW_ECommerceBaseVenta>()
                .Property(e => e.cLinea)
                .IsUnicode(false);

            modelBuilder.Entity<VW_ECommerceBaseVenta>()
                .Property(e => e.cModelo)
                .IsUnicode(false);

            modelBuilder.Entity<VW_ECommerceBaseVenta>()
                .Property(e => e.cDescripcion)
                .IsUnicode(false);

            modelBuilder.Entity<VW_ECommerceBaseVenta>()
                .Property(e => e.nPrecioBase)
                .HasPrecision(19, 4);

            modelBuilder.Entity<VW_ECommerceBaseVenta>()
                .Property(e => e.nPrecioVenta)
                .HasPrecision(19, 4);


            modelBuilder.Entity<VW_ECommerceFamilia>()
                .Property(e => e.cDescripcion)
                .IsUnicode(false);

            modelBuilder.Entity<VW_ECommerceLinea>()
                .Property(e => e.cDescripcion)
                .IsUnicode(false);

            modelBuilder.Entity<VW_ECommerceMarca>()
                .Property(e => e.cDescripcion)
                .IsUnicode(false);

            modelBuilder.Entity<VW_ECommerceModelo>()
                .Property(e => e.cDescripcion)
                .IsUnicode(false);

            modelBuilder.Entity<reflejoArticulo>()
              .Property(e => e.cSKU)
              .IsUnicode(false);

            modelBuilder.Entity<reflejoArticulo>()
                .Property(e => e.nPrecioBase)
                .HasPrecision(19, 4);

            modelBuilder.Entity<reflejoArticulo>()
                .Property(e => e.nPrecioVenta)
                .HasPrecision(19, 4);

            modelBuilder.Entity<reflejoArticulo>()
                .Property(e => e.cDescripcion)
                .IsUnicode(false);

            modelBuilder.Entity<usuarios>()
                           .Property(e => e.nombres)
                           .IsUnicode(false);

            modelBuilder.Entity<usuarios>()
                .Property(e => e.apellidos)
                .IsUnicode(false);

            modelBuilder.Entity<usuarios>()
                .Property(e => e.dni)
                .IsUnicode(false);

            modelBuilder.Entity<usuarios>()
                .Property(e => e.correo)
                .IsUnicode(false);

            modelBuilder.Entity<usuarios>()
                .Property(e => e.telefono)
                .IsUnicode(false);

            modelBuilder.Entity<usuarios>()
                .Property(e => e.cSKU)
                .IsUnicode(false);


            modelBuilder.Entity<articuloImagen>()
               .Property(e => e.sku)
               .IsUnicode(false); 

            modelBuilder.Entity<VW_ECommerceAgencias>()
               .Property(e => e.cAgencia)
               .IsUnicode(false);

            modelBuilder.Entity<VW_ECommerceAgencias>()
                .Property(e => e.cDepartamento)
                .IsUnicode(false);

            modelBuilder.Entity<VW_ECommerceAgencias>()
                .Property(e => e.cDireccion)
                .IsUnicode(false);

            modelBuilder.Entity<VW_ECommerceAgencias>()
                .Property(e => e.cTelefono)
                .IsUnicode(false);

            modelBuilder.Entity<VW_ECommerceAgencias>()
                .Property(e => e.cHorarioLV)
                .IsUnicode(false);

            modelBuilder.Entity<VW_ECommerceAgencias>()
                .Property(e => e.cHorarioS)
                .IsUnicode(false);

            modelBuilder.Entity<VW_ECommerceAgencias>()
                .Property(e => e.cHorarioD)
                .IsUnicode(false);

            modelBuilder.Entity<VW_ECommerceClientes>()
                .Property(e => e.cDNI)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<VW_ECommerceClientes>()
                .Property(e => e.cNombres)
                .IsUnicode(false);

            modelBuilder.Entity<VW_ECommerceClientes>()
                .Property(e => e.cApellidos)
                .IsUnicode(false);

            modelBuilder.Entity<articulo_visto>()
               .Property(e => e.cSKU)
               .IsUnicode(false);

            modelBuilder.Entity<articulo_separado>()
                .Property(e => e.cSKU)
                .IsUnicode(false);

            modelBuilder.Entity<estado>()
                .Property(e => e.descripcion)
                .IsUnicode(false);

        }
    }
}
