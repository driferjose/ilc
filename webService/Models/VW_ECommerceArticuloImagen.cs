﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.Spatial;

namespace webService.Models
{
    [Table("PER.VW_ECommerceArticuloImagenSIFIC")]
    public class VW_ECommerceArticuloImagen
    {
        [Key]
        [StringLength(20)]
        public string cSKU { get; set; }

        [Column(TypeName = "image")]
        public byte[] iFoto1 { get; set; }

        [Column(TypeName = "image")]
        public byte[] iFoto2 { get; set; }

        [Column(TypeName = "image")]
        public byte[] iFoto3 { get; set; }

        [Column(TypeName = "image")]
        public byte[] iFoto4 { get; set; }

        [Column(TypeName = "image")]
        public byte[] iFoto5 { get; set; }

        [Column(TypeName = "image")]
        public byte[] iFoto6 { get; set; }
    }
}