namespace webService.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("PER.articulo_visto")]
    public partial class articulo_visto
    {
        [Key]
        public int idArticuloVisto { get; set; }

        [StringLength(50)]
        public string cSKU { get; set; }

        [StringLength(50)]
        public string nombre { get; set; }

        [StringLength(250)]
        public string descripcion { get; set; }

        [Column(TypeName = "money")]
        public decimal? precioBase { get; set; }

        [Column(TypeName = "money")]
        public decimal? precioVenta { get; set; }
       
        [Column(TypeName = "datetime2")]
        public DateTime? fecha { get; set; }

        [StringLength(50)]
        public string agencia { get; set; }
    }
}
