namespace webService.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("PER.estado")]
    public partial class estado
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int idEstado { get; set; }

        [StringLength(50)]
        public string descripcion { get; set; }
    }
}
