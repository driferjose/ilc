namespace webService.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("PER.tiemposReserva")]
    public partial class tiemposReserva
    {
        [Key]
        public int idTiempoReserva { get; set; }

        [StringLength(250)]
        public string ubicacion { get; set; }

        public int? tiempoReserva { get; set; }

        public int? dias { get; set; }
    }
}
