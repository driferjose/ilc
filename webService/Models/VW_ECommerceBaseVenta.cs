namespace webService.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("PER.VW_ECommerceBaseVenta")]
    public partial class VW_ECommerceBaseVenta
    {
        [Key]
        [Column(Order = 0)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int nIdArticulo { get; set; }

        [StringLength(50)]
        public string cSKU { get; set; }

        public int? nCodFamilia { get; set; }

        [Key]
        [Column(Order = 1)]
        [StringLength(500)]
        public string cFamilia { get; set; }

        public int? nCodArticulo { get; set; }

        [Key]
        [Column(Order = 2)]
        [StringLength(500)]
        public string cArticulo { get; set; }

        public int? nCodMarca { get; set; }

        [Key]
        [Column(Order = 3)]
        [StringLength(500)]
        public string cMarca { get; set; }

        public int? nCodLinea { get; set; }

        [Key]
        [Column(Order = 4)]
        [StringLength(500)]
        public string cLinea { get; set; }

        public int? nCodModelo { get; set; }

        [Key]
        [Column(Order = 5)]
        [StringLength(500)]
        public string cModelo { get; set; }

        public string cDescripcion { get; set; }

        [Column(TypeName = "money")]
        public decimal? nPrecioBase { get; set; }

        [Column(TypeName = "money")]
        public decimal? nPrecioVenta { get; set; }

        [Key]
        [Column(Order = 6)]
        public int nCodAge { get; set; }

        public string cObservaciones { get; set; }

        [Column(TypeName = "image")]
        public byte[] oImagen1 { get; set; }

        [Column(TypeName = "image")]
        public byte[] oImagen2 { get; set; }

        [Column(TypeName = "image")]
        public byte[] oImagen3 { get; set; }

        [Column(TypeName = "image")]
        public byte[] oImagen4 { get; set; }

        [Column(TypeName = "image")]
        public byte[] oImagen5 { get; set; }

        [Column(TypeName = "image")]
        public byte[] oImagen6 { get; set; }
    }
}
