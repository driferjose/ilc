namespace webService.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("PER.ofertasArticulos")]
    public partial class ofertasArticulo
    {
        [Key]
        [Column(Order = 0)]
        public int nIdOferta { get; set; }

        public string nSkuArticulo { get; set; }

        public int? nCodArticulo { get; set; }

        [Column(TypeName = "money")]
        public decimal? nPrecioOFerta { get; set; }

        [Key]
        [Column(Order = 1)]
        public bool nEstado { get; set; }
    }
}
