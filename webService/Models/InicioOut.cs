﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace webService.Models
{
    public class InicioOut
    {
        public List<FamiliaModelo> ArrayFamilias  { get; set; }
        public IQueryable Marcas { get; set; }
        public List<RangoInit> RangoPrecios { get; set; }
        //public List<UbicacionesInit> Ubicaciones { get; set; }
    }

    public class RangoInit
    {
        public int idRangoPrecio { get; set; }
        public string nombreRango { get; set; }
    }

    /*public class UbicacionesInit
    {
        public int id { get; set; }
        public string ubicacion { get; set; }
    } */


}