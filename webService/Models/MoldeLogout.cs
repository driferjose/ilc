﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace webService.Models
{
    public class MoldeLogout
    {
        public string usuario { get; set; }
        public string password { get; set; }
        public string token { get; set; }
    }
}