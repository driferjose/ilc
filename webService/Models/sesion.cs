namespace webService.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("PER.sesion")]
    public partial class sesion
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public sesion()
        {
            intentos_separar = new HashSet<intentos_separar>();
            navegacions = new HashSet<navegacion>();
        }

        [Key]
        public int idSesion { get; set; }

        [StringLength(250)]
        public string token { get; set; }

        [StringLength(250)]
        public string usuario { get; set; }

        public int? codAgencia { get; set; }

        [Column(TypeName = "datetime2")]
        public DateTime? fechaCreacion { get; set; }

        [Column(TypeName = "datetime2")]
        public DateTime? fechaSalida { get; set; }

        [StringLength(50)]
        public string usuarioSalida { get; set; }

        public bool? activo { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<intentos_separar> intentos_separar { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<navegacion> navegacions { get; set; }
    }
}
