namespace webService.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("PER.admin_sesion")]
    public partial class admin_sesion
    {
        [Key]
        public int idSesionAdmin { get; set; }

        public int? idUsuario { get; set; }

        [Column(TypeName = "date")]
        public DateTime? fechaSesion { get; set; }

        [StringLength(250)]
        public string token { get; set; }

        [Column(TypeName = "date")]
        public DateTime? fechaCierreSesion { get; set; }
        
    }
}
