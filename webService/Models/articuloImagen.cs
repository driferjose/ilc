namespace webService.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("PER.articuloImagen")]
    public partial class articuloImagen
    {
        public int id { get; set; }

        [Required]
        [StringLength(50)]
        public string sku { get; set; }

        [StringLength(250)]
        public string imgSizeSmall { get; set; }

        [StringLength(250)]
        public string imgSizeMediun { get; set; }

        [StringLength(250)]
        public string imgSizeOriginal { get; set; }

        public bool? activo { get; set; }

        public bool? predeterminado { get; set; }
    }
}
