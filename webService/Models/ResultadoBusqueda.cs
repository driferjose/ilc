﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace webService.Models
{
    public class ResultadoBusqueda
    {
        
        public string sku { get; set; }

       
        public string nombre { get; set; }

      
        public decimal? precioNormal { get; set; }

        public decimal? precioVenta { get; set; }

        public string imagen { get; set; }

        public string descripcion { get; set; }

        public string marca { get; set; }

        public string familia { get; set; }

    }
}