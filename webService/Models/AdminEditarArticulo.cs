﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Web;

namespace webService.Models
{
    public class AdminEditarArticulo
    {
        public string nombreArticulo { get; set; }
        public decimal? precioNormal { get; set; }
        public decimal? precioVenta { get; set; }
        public string descripcion { get; set; }
        public string observaciones { get; set; }
        public string token { get; set; }
        public string sku { get; set; }
        public bool? oferta { get; set; }
        public int[] imgElimanadas { get; set; }
    }
}
