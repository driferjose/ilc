﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace webService.Models
{
    public class AdminRespuestaFiltroArticulo
    {
        public string nombreArticulo { get; set; }
        public string agencia { get; set; }
        public string sku { get; set; }
    }
}