﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace webService.Models
{
    public class MoldeEnvioSMS
    {
        public string token { get; set; }
        public string telefono { get; set; }
        public string dni { get; set; }
        public string sku { get; set; }
        public string nombreArticulo { get; set; }
        public int codAgencia { get; set; }
    }
}