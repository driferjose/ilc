﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace webService.Models
{
    public class MoldePeticionFiltro
    {
        public int familia { get; set; }
        public string marca  { get; set; }
        public int? rango { get; set; }
        public int codAgencia { get; set; }
        public string token { get; set; }
    }
}