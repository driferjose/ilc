﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace webService.Models
{
    public class ReporteUsuarioRegistrado
    {
       
        public string nombres { get; set; }

       
        public string apellidos { get; set; }

       
        public string dni { get; set; }

        
        public string correo { get; set; }

        public string telefono { get; set; }
        
        public DateTime? fechaRegistro { get; set; }

        public string agencia { get; set; }

    }
}