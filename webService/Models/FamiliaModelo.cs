﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace webService.Models
{
    public class FamiliaModelo
    {
        public string Nombre { get; set; }
        public int? Codigo { get; set; }
        public string IconoUrl { get; set; }
        public string[] ArregloMarcas { get; set; }
        public List<ResultadoFamilias> CodUbicacion { get; set; }

        public FamiliaModelo( string nombre , int? codigo, string iconoUrl ,string[] arreglo, List<ResultadoFamilias> codUbicacion )
        {
            Nombre = nombre;
            Codigo = codigo;
            IconoUrl = iconoUrl;
            ArregloMarcas = arreglo;
            CodUbicacion = codUbicacion;

        }
    }
}