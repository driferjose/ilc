﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace webService.Models
{
    public class MoldeSeparacion
    {
        public string dni { get; set; }
        public string cSKU { get; set; }
        public string telefono { get; set; }
        public string correo { get; set; }
        public string token { get; set; }
    }
}