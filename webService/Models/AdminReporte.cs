﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace webService.Models
{
    public class AdminReporte
    {

        public string inicio { get; set; }
        public string  fin { get; set; }
        public int codAgencia { get; set; }
        public int? indicador { get; set; }
        public string token { get; set; }
    }
}