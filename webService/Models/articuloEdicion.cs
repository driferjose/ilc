namespace webService.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("PER.articuloEdicion")]
    public partial class articuloEdicion
    {
        [Key]
        public int idHistoricoEdicion { get; set; }

        public int? idAdminSesion { get; set; }

        [StringLength(50)]
        public string articuloEditado { get; set; }

        [Column(TypeName = "datetime2")]
        public DateTime? fechaEdicion { get; set; }
    }
}
