﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace webService.Models
{
    public class MoldeEnvioEmail
    {
        public string correo { get; set; }
        public string sku { get; set; }
        public string nombre { get; set; }
        public int codAgencia { get; set; }
        public string token { get; set; }
    }
}