﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace webService.Models
{
    public class RegistroUsuario
    {
      
        public int idUsuario { get; set; }

       
        public string nombres { get; set; }

      
        public string apellidos { get; set; }

       
        public string dni { get; set; }

        
        public string correo { get; set; }

       
        public string telefono { get; set; }

       
        public DateTime? fechaCreacion { get; set; }

       
        public string cSKU { get; set; }

     
        public DateTime? reservaExpiracion { get; set; }

        public string token { get; set; }
    }
}