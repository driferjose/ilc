namespace webService.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("PER.servicioSMS")]
    public partial class servicioSMS
    {
        public int id { get; set; }

        [StringLength(50)]
        public string usuario { get; set; }

        [StringLength(250)]
        public string clave { get; set; }

        [StringLength(250)]
        public string url { get; set; }
    }
}
