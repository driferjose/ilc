namespace webService.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("PER.usuarios")]
    public partial class usuarios
    {
        [Key]
        public int idUsuario { get; set; }

        [StringLength(250)]
        public string nombres { get; set; }

        [StringLength(250)]
        public string apellidos { get; set; }

        [StringLength(8)]
        public string dni { get; set; }

        [StringLength(45)]
        public string correo { get; set; }

        [StringLength(9)]
        public string telefono { get; set; }

        [Column(TypeName = "date")]
        public DateTime? fechaCreacion { get; set; }

        [StringLength(50)]
        public string cSKU { get; set; }

        [Column(TypeName = "datetime2")]
        public DateTime? reservaExpiracion { get; set; }

        public int? idSesion { get; set; }
        
    }
}
