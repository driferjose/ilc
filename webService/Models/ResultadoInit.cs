﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace webService.Models
{
    public class ResultadoInit
    {
        public int codigo { get; set; }
        public string nombre { get; set; }
        public string imgUrl { get; set; }
        public decimal precioMax { get; set; }
    }
}