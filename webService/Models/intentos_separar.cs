namespace webService.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("PER.intentos_separar")]
    public partial class intentos_separar
    {
        [Key]
        public int idIntento { get; set; }

        public int? idSesion { get; set; }

        [Column(TypeName = "datetime2")]
        public DateTime? fecha { get; set; }

        public int? idArticulo { get; set; }

        public virtual sesion sesion { get; set; }
    }
}
