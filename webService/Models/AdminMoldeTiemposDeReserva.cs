﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace webService.Models
{
    public class AdminMoldeTiemposDeReserva
    {
        public string Lugar { get; set; }
        public int? Dias { get; set; }

        public AdminMoldeTiemposDeReserva(string lugar , int? dias )
        {
            Lugar = lugar;
            Dias = dias;
        }
    }
}