﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace webService.Models
{
    public class FiltroCategorias
    {
        public string cSKU { get; set; }
        public string cArticulo { get; set; }
        public string imgSizeSmall { get; set; }
        public decimal? nPrecioVenta { get; set; }
        public decimal? nPrecioOFerta { get; set; }
    }
}