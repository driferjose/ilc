namespace webService.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("PER.indicador")]
    public partial class indicador
    {
        [Key]
        public int idIndicador { get; set; }

        [StringLength(50)]
        public string descripcion { get; set; }
    }
}
