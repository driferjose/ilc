﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace webService.Models
{
    public class MoldeFiltroFamilias
    {
        public int codAgencia { get; set; }
        public int codFamilia { get; set; }
        public string nombre { get; set; }
    }
}