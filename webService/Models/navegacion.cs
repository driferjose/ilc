namespace webService.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("PER.navegacion")]
    public partial class navegacion
    {
        [Key]
        public int idNavegacion { get; set; }

        public int? idSesion { get; set; }

        public int? nCodFamilia { get; set; }

        [StringLength(500)]
        public string cMarca { get; set; }

        public int? nIdRango { get; set; }

        [Column(TypeName = "datetime2")]
        public DateTime? fechaConsulta { get; set; }

        public virtual sesion sesion { get; set; }
    }
}
