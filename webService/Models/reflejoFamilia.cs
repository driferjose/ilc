namespace webService.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("PER.reflejoFamilia")]
    public partial class reflejoFamilia
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int nCodFamilia { get; set; }

        [StringLength(250)]
        public string iconoUrl { get; set; }
    }
}
