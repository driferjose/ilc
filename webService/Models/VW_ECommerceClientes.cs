namespace webService.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("PER.VW_ECommerceClientes")]
    public partial class VW_ECommerceClientes
    {
        [Key]
        [Column(Order = 0)]
        [StringLength(44)]
        public string cDNI { get; set; }

        [Key]
        [Column(Order = 1)]
        [StringLength(100)]
        public string cNombres { get; set; }

        [Key]
        [Column(Order = 2)]
        [StringLength(201)]
        public string cApellidos { get; set; }

        public bool? Es_CLIENTE { get; set; }
    }
}
