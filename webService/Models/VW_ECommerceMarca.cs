namespace webService.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("PER.VW_ECommerceMarca")]
    public partial class VW_ECommerceMarca
    {
        [Key]
        public int nCodMarca { get; set; }

        [StringLength(500)]
        public string cDescripcion { get; set; }

        public bool? bActivo { get; set; }
    }
}
