namespace webService.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("PER.reflejoBaseVenta")]
    public partial class reflejoBaseVenta
    {
       
        public int? nIdArticulo { get; set; }

        [Key]
        [StringLength(50)]
        public string cSKU { get; set; }

        [StringLength(500)]
        public string cFamilia { get; set; }

       
        public int? nCodFamilia { get; set; }

        public int? nCodArticulo { get; set; }

        [StringLength(500)]
        public string cArticulo { get; set; }

        public int? nCodMarca { get; set; }

        [StringLength(500)]
        public string cMarca { get; set; }

        public int? nCodLinea { get; set; }

        [StringLength(500)]
        public string cLinea { get; set; }

        public int? nCodModelo { get; set; }

        [StringLength(500)]
        public string cModelo { get; set; }

        public string cDescripcion { get; set; }

        [Column(TypeName = "money")]
        public decimal? nPrecioBase { get; set; }

        [Column(TypeName = "money")]
        public decimal? nPrecioVenta { get; set; }

        

        public int? nCodAge { get; set; }

        public string cObservaciones { get; set; }
        

        public bool? oferta { get; set; }

        public int? estado { get; set; }
    }
}
