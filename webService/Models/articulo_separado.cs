namespace webService.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("PER.articulo_separado")]
    public partial class articulo_separado
    {
        [Key]
        public int idArticuloSeparado { get; set; }

        [StringLength(50)]
        public string cSKU { get; set; }

        [StringLength(100)]
        public string nombre { get; set; }

        public string descripcion { get; set; }

        [Column(TypeName = "money")]
        public decimal? precioBase { get; set; }

        [Column(TypeName = "money")]
        public decimal? precioVenta { get; set; }

        [Column(TypeName = "datetime2")]
        public DateTime? fecha { get; set; }

        public int codAgencia { get; set; }

        [StringLength(8)]
        public string dni { get; set; }

        [StringLength(100)]
        public string telefono { get; set; }

        [StringLength(100)]
        public string correo { get; set; }

        [StringLength(20)]
        public string fechaRecojo { get; set; }
    }
}
