﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace webService.Models
{
    public class AdminFiltroArticulos
    {
        public bool editado { get; set; }
        public bool oferta { get; set; }
        public int agencia { get; set; }
        public string sku { get; set; }
        public string token { get; set; }
        public bool eliminado { get; set; }

    }
}