﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace webService.Models
{
    public class ReporteArticuloReservado
    {
        public string sku { get; set; }
        public string articulo { get; set; }
        public string marca { get; set; }
        public string descripcionArticulo { get; set; }
        public decimal? precioVenta { get; set; }
        public string dni { get; set; }
        public string nombre { get; set; }
        public string apellidos { get; set; }
        public string correo { get; set; }
        public string celular { get; set; }
        public DateTime? fechaReserva { get; set; }
        public int? codAgencia { get; set; }
        public string nombreAgencia { get; set; }
        public string cliente { get; set; }
    }
}