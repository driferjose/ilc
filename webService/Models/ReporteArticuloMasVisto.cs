﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace webService.Models
{
    public class ReporteArticuloMasVisto
    {
       
        public string cSKU { get; set; }

        
        public string nombre { get; set; }

        public string marca { get; set; }

        public string descripcion { get; set; }

     
        public decimal? precioBase { get; set; }

       
        public decimal? precioVenta { get; set; }

       
        public DateTime? fecha { get; set; }

       
        public string agencia { get; set; }

        public int total { get; set; }
    }
}