USE [DBECommerce]
GO
ALTER TABLE [PER].[navegacion] DROP CONSTRAINT [FK_sesion]
GO
ALTER TABLE [PER].[navegacion] DROP CONSTRAINT [FK_rangoPrecio]
GO
ALTER TABLE [PER].[intentos_separar] DROP CONSTRAINT [FK_sesion_separacion]
GO
/****** Object:  Index [idx_sku]    Script Date: 7/10/2017 12:38:22 PM ******/
DROP INDEX [idx_sku] ON [PER].[reflejoBaseVenta]
GO
/****** Object:  Index [idx_precioVenta]    Script Date: 7/10/2017 12:38:22 PM ******/
DROP INDEX [idx_precioVenta] ON [PER].[reflejoBaseVenta]
GO
/****** Object:  Index [idx_codFamilia]    Script Date: 7/10/2017 12:38:22 PM ******/
DROP INDEX [idx_codFamilia] ON [PER].[reflejoBaseVenta]
GO
/****** Object:  Index [idx_codAgencia]    Script Date: 7/10/2017 12:38:22 PM ******/
DROP INDEX [idx_codAgencia] ON [PER].[reflejoBaseVenta]
GO
/****** Object:  Index [idx_cMarca]    Script Date: 7/10/2017 12:38:22 PM ******/
DROP INDEX [idx_cMarca] ON [PER].[reflejoBaseVenta]
GO
/****** Object:  Table [PER].[usuarios]    Script Date: 7/10/2017 12:38:22 PM ******/
DROP TABLE [PER].[usuarios]
GO
/****** Object:  Table [PER].[tiemposReserva]    Script Date: 7/10/2017 12:38:22 PM ******/
DROP TABLE [PER].[tiemposReserva]
GO
/****** Object:  Table [PER].[sesion]    Script Date: 7/10/2017 12:38:22 PM ******/
DROP TABLE [PER].[sesion]
GO
/****** Object:  Table [PER].[servicioSMS]    Script Date: 7/10/2017 12:38:22 PM ******/
DROP TABLE [PER].[servicioSMS]
GO
/****** Object:  Table [PER].[reflejoFamilia]    Script Date: 7/10/2017 12:38:22 PM ******/
DROP TABLE [PER].[reflejoFamilia]
GO
/****** Object:  Table [PER].[reflejoBaseVenta]    Script Date: 7/10/2017 12:38:22 PM ******/
DROP TABLE [PER].[reflejoBaseVenta]
GO
/****** Object:  Table [PER].[rangoPrecios]    Script Date: 7/10/2017 12:38:22 PM ******/
DROP TABLE [PER].[rangoPrecios]
GO
/****** Object:  Table [PER].[ofertasArticulos]    Script Date: 7/10/2017 12:38:22 PM ******/
DROP TABLE [PER].[ofertasArticulos]
GO
/****** Object:  Table [PER].[navegacion]    Script Date: 7/10/2017 12:38:22 PM ******/
DROP TABLE [PER].[navegacion]
GO
/****** Object:  Table [PER].[intentos_separar]    Script Date: 7/10/2017 12:38:22 PM ******/
DROP TABLE [PER].[intentos_separar]
GO
/****** Object:  Table [PER].[indicador]    Script Date: 7/10/2017 12:38:22 PM ******/
DROP TABLE [PER].[indicador]
GO
/****** Object:  Table [PER].[estado]    Script Date: 7/10/2017 12:38:22 PM ******/
DROP TABLE [PER].[estado]
GO
/****** Object:  Table [PER].[articuloImagen]    Script Date: 7/10/2017 12:38:22 PM ******/
DROP TABLE [PER].[articuloImagen]
GO
/****** Object:  Table [PER].[articuloEdicion]    Script Date: 7/10/2017 12:38:22 PM ******/
DROP TABLE [PER].[articuloEdicion]
GO
/****** Object:  Table [PER].[articulo_visto]    Script Date: 7/10/2017 12:38:22 PM ******/
DROP TABLE [PER].[articulo_visto]
GO
/****** Object:  Table [PER].[articulo_separado]    Script Date: 7/10/2017 12:38:22 PM ******/
DROP TABLE [PER].[articulo_separado]
GO
/****** Object:  Table [PER].[admin_usuario]    Script Date: 7/10/2017 12:38:22 PM ******/
DROP TABLE [PER].[admin_usuario]
GO
/****** Object:  Table [PER].[admin_token]    Script Date: 7/10/2017 12:38:22 PM ******/
DROP TABLE [PER].[admin_token]
GO
/****** Object:  Table [PER].[admin_sesion]    Script Date: 7/10/2017 12:38:22 PM ******/
DROP TABLE [PER].[admin_sesion]
GO
/****** Object:  Table [PER].[admin_sesion]    Script Date: 7/10/2017 12:38:22 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [PER].[admin_sesion](
	[idSesionAdmin] [int] IDENTITY(1,1) NOT NULL,
	[idUsuario] [int] NULL,
	[fechaSesion] [date] NULL,
	[token] [varchar](250) NULL,
	[fechaCierreSesion] [date] NULL,
 CONSTRAINT [PK_admin_sesion] PRIMARY KEY CLUSTERED 
(
	[idSesionAdmin] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [PER].[admin_token]    Script Date: 7/10/2017 12:38:23 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [PER].[admin_token](
	[token] [varchar](250) NULL
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [PER].[admin_usuario]    Script Date: 7/10/2017 12:38:23 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [PER].[admin_usuario](
	[idAdminUsuario] [int] IDENTITY(1,1) NOT NULL,
	[usuario] [varchar](50) NULL,
	[password] [varchar](250) NULL,
 CONSTRAINT [PK_admin_usuario] PRIMARY KEY CLUSTERED 
(
	[idAdminUsuario] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [PER].[articulo_separado]    Script Date: 7/10/2017 12:38:23 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [PER].[articulo_separado](
	[idArticuloSeparado] [int] IDENTITY(1,1) NOT NULL,
	[cSKU] [varchar](50) NULL,
	[nombre] [varchar](100) NULL,
	[descripcion] [varchar](max) NULL,
	[precioBase] [money] NULL,
	[precioVenta] [money] NULL,
	[fecha] [datetime2](7) NULL,
	[codAgencia] [int] NULL,
	[dni] [varchar](8) NULL,
	[telefono] [varchar](100) NULL,
	[correo] [varchar](100) NULL,
	[fechaRecojo] [varchar](20) NULL,
 CONSTRAINT [PK_articulo_separado] PRIMARY KEY CLUSTERED 
(
	[idArticuloSeparado] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [PER].[articulo_visto]    Script Date: 7/10/2017 12:38:23 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [PER].[articulo_visto](
	[idArticuloVisto] [int] IDENTITY(1,1) NOT NULL,
	[cSKU] [varchar](50) NULL,
	[nombre] [varchar](50) NULL,
	[descripcion] [varchar](250) NULL,
	[precioBase] [money] NULL,
	[precioVenta] [money] NULL,
	[fecha] [datetime2](7) NULL,
	[agencia] [varchar](50) NULL,
 CONSTRAINT [PK_articulo_mas_visto] PRIMARY KEY CLUSTERED 
(
	[idArticuloVisto] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [PER].[articuloEdicion]    Script Date: 7/10/2017 12:38:23 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [PER].[articuloEdicion](
	[idHistoricoEdicion] [int] IDENTITY(1,1) NOT NULL,
	[idAdminSesion] [int] NULL,
	[articuloEditado] [varchar](50) NULL,
	[fechaEdicion] [datetime2](7) NULL,
 CONSTRAINT [PK_articuloEdicion] PRIMARY KEY CLUSTERED 
(
	[idHistoricoEdicion] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [PER].[articuloImagen]    Script Date: 7/10/2017 12:38:23 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [PER].[articuloImagen](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[sku] [varchar](50) NOT NULL,
	[imgSizeSmall] [varchar](250) NULL,
	[imgSizeMediun] [varchar](250) NULL,
	[imgSizeOriginal] [varchar](250) NULL,
	[activo] [bit] NULL,
	[predeterminado] [bit] NULL,
 CONSTRAINT [PK_articuloImagen] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [PER].[estado]    Script Date: 7/10/2017 12:38:23 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [PER].[estado](
	[idEstado] [int] NOT NULL,
	[descripcion] [varchar](50) NULL,
 CONSTRAINT [PK_estado] PRIMARY KEY CLUSTERED 
(
	[idEstado] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [PER].[indicador]    Script Date: 7/10/2017 12:38:23 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [PER].[indicador](
	[idIndicador] [int] IDENTITY(1,1) NOT NULL,
	[descripcion] [varchar](50) NULL,
 CONSTRAINT [PK_indicador] PRIMARY KEY CLUSTERED 
(
	[idIndicador] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [PER].[intentos_separar]    Script Date: 7/10/2017 12:38:23 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [PER].[intentos_separar](
	[idIntento] [int] IDENTITY(1,1) NOT NULL,
	[idSesion] [int] NULL,
	[fecha] [datetime2](7) NULL,
	[idArticulo] [int] NULL,
 CONSTRAINT [PK_intentos_separar] PRIMARY KEY CLUSTERED 
(
	[idIntento] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [PER].[navegacion]    Script Date: 7/10/2017 12:38:23 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [PER].[navegacion](
	[idNavegacion] [int] IDENTITY(1,1) NOT NULL,
	[idSesion] [int] NULL,
	[nCodFamilia] [int] NULL,
	[cMarca] [varchar](500) NULL,
	[nIdRango] [int] NULL,
	[fechaConsulta] [datetime2](7) NULL,
 CONSTRAINT [PK_navegacion] PRIMARY KEY CLUSTERED 
(
	[idNavegacion] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [PER].[ofertasArticulos]    Script Date: 7/10/2017 12:38:23 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [PER].[ofertasArticulos](
	[nIdOferta] [int] IDENTITY(1,1) NOT NULL,
	[nSkuArticulo] [varchar](50) NULL,
	[nCodArticulo] [int] NULL,
	[nPrecioOFerta] [money] NULL,
	[nEstado] [bit] NOT NULL
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [PER].[rangoPrecios]    Script Date: 7/10/2017 12:38:23 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [PER].[rangoPrecios](
	[nIdRango] [int] IDENTITY(1,1) NOT NULL,
	[cNombreRango] [varchar](50) NULL,
	[cValorMinimo] [int] NULL,
	[cValorMaximo] [int] NULL,
	[cEstado] [bit] NOT NULL,
 CONSTRAINT [PK_rangoPrecios] PRIMARY KEY CLUSTERED 
(
	[nIdRango] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [PER].[reflejoBaseVenta]    Script Date: 7/10/2017 12:38:23 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [PER].[reflejoBaseVenta](
	[nIdArticulo] [int] NULL,
	[cSKU] [varchar](50) NOT NULL,
	[cFamilia] [varchar](500) NULL,
	[nCodFamilia] [int] NULL,
	[nCodArticulo] [int] NULL,
	[cArticulo] [varchar](500) NULL,
	[nCodMarca] [int] NULL,
	[cMarca] [varchar](500) NULL,
	[nCodLinea] [int] NULL,
	[cLinea] [varchar](500) NULL,
	[nCodModelo] [int] NULL,
	[cModelo] [varchar](500) NULL,
	[cDescripcion] [varchar](max) NULL,
	[nPrecioBase] [money] NULL,
	[nPrecioVenta] [money] NULL,
	[nCodAge] [int] NULL,
	[cObservaciones] [varchar](max) NULL,
	[oferta] [bit] NULL,
	[estado] [int] NULL,
 CONSTRAINT [PK_reflejoBaseVenta_1] PRIMARY KEY CLUSTERED 
(
	[cSKU] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [PER].[reflejoFamilia]    Script Date: 7/10/2017 12:38:23 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [PER].[reflejoFamilia](
	[nCodFamilia] [int] NOT NULL,
	[iconoUrl] [varchar](250) NULL
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [PER].[servicioSMS]    Script Date: 7/10/2017 12:38:23 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [PER].[servicioSMS](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[usuario] [varchar](50) NULL,
	[clave] [varchar](250) NULL,
	[url] [varchar](250) NULL,
 CONSTRAINT [PK_servicioSMS] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [PER].[sesion]    Script Date: 7/10/2017 12:38:23 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [PER].[sesion](
	[idSesion] [int] IDENTITY(1,1) NOT NULL,
	[token] [varchar](250) NULL,
	[usuario] [varchar](250) NULL,
	[codAgencia] [int] NULL,
	[fechaCreacion] [datetime2](7) NULL,
	[fechaSalida] [datetime2](7) NULL,
	[usuarioSalida] [varchar](50) NULL,
	[activo] [bit] NULL,
 CONSTRAINT [PK_sesion] PRIMARY KEY CLUSTERED 
(
	[idSesion] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [PER].[tiemposReserva]    Script Date: 7/10/2017 12:38:23 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [PER].[tiemposReserva](
	[idTiempoReserva] [int] IDENTITY(1,1) NOT NULL,
	[ubicacion] [varchar](250) NULL,
	[tiempoReserva] [int] NULL,
	[dias] [int] NULL,
 CONSTRAINT [PK_tiemposReserva] PRIMARY KEY CLUSTERED 
(
	[idTiempoReserva] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [PER].[usuarios]    Script Date: 7/10/2017 12:38:23 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [PER].[usuarios](
	[idUsuario] [int] IDENTITY(1,1) NOT NULL,
	[nombres] [varchar](250) NULL,
	[apellidos] [varchar](250) NULL,
	[dni] [varchar](8) NULL,
	[correo] [varchar](45) NULL,
	[telefono] [varchar](9) NULL,
	[fechaCreacion] [datetime2](7) NULL,
	[cSKU] [varchar](50) NULL,
	[reservaExpiracion] [date] NULL,
	[idSesion] [int] NULL,
 CONSTRAINT [PK_usuarios] PRIMARY KEY CLUSTERED 
(
	[idUsuario] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [idx_cMarca]    Script Date: 7/10/2017 12:38:23 PM ******/
CREATE NONCLUSTERED INDEX [idx_cMarca] ON [PER].[reflejoBaseVenta]
(
	[cMarca] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [idx_codAgencia]    Script Date: 7/10/2017 12:38:23 PM ******/
CREATE NONCLUSTERED INDEX [idx_codAgencia] ON [PER].[reflejoBaseVenta]
(
	[nCodAge] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [idx_codFamilia]    Script Date: 7/10/2017 12:38:23 PM ******/
CREATE NONCLUSTERED INDEX [idx_codFamilia] ON [PER].[reflejoBaseVenta]
(
	[nCodFamilia] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [idx_precioVenta]    Script Date: 7/10/2017 12:38:23 PM ******/
CREATE NONCLUSTERED INDEX [idx_precioVenta] ON [PER].[reflejoBaseVenta]
(
	[nPrecioVenta] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [idx_sku]    Script Date: 7/10/2017 12:38:23 PM ******/
CREATE NONCLUSTERED INDEX [idx_sku] ON [PER].[reflejoBaseVenta]
(
	[cSKU] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
ALTER TABLE [PER].[intentos_separar]  WITH CHECK ADD  CONSTRAINT [FK_sesion_separacion] FOREIGN KEY([idSesion])
REFERENCES [PER].[sesion] ([idSesion])
GO
ALTER TABLE [PER].[intentos_separar] CHECK CONSTRAINT [FK_sesion_separacion]
GO
ALTER TABLE [PER].[navegacion]  WITH CHECK ADD  CONSTRAINT [FK_rangoPrecio] FOREIGN KEY([nIdRango])
REFERENCES [PER].[rangoPrecios] ([nIdRango])
GO
ALTER TABLE [PER].[navegacion] CHECK CONSTRAINT [FK_rangoPrecio]
GO
ALTER TABLE [PER].[navegacion]  WITH CHECK ADD  CONSTRAINT [FK_sesion] FOREIGN KEY([idSesion])
REFERENCES [PER].[sesion] ([idSesion])
GO
ALTER TABLE [PER].[navegacion] CHECK CONSTRAINT [FK_sesion]
GO
