USE [DBECommerce]
GO
/****** Object:  StoredProcedure [PER].[MigraDataVenta]    Script Date: 7/10/2017 2:57:34 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [PER].[MigraDataVenta]
as
begin

INSERT INTO  [PER].[reflejoBaseVenta]
( [nIdArticulo], [cSKU], [cFamilia], [nCodFamilia] ,[nCodArticulo] ,[cArticulo] ,[nCodMarca]
,[cMarca] ,[nCodLinea], [cLinea], [nCodModelo], [cModelo], [cDescripcion], [nPrecioBase], [nPrecioVenta],
[nCodAge], [cObservaciones],[oferta], estado )
SELECT 
[nIdArticulo], [cSKU], [cFamilia],[nCodFamilia],[nCodArticulo], [cArticulo], [nCodMarca], [cMarca],
[nCodLinea], [cLinea], [nCodModelo], [cModelo], [cDescripcion], [nPrecioBase], [nPrecioVenta], 
b.[nCodAge], [cObservaciones],0,2
FROM [PER].[VW_ECommerceBaseVenta] B
JOIN [PER].[VW_ECommerceAgencias] C ON B.[nCodAge] = C.[nCodAge]
WHERE NOT EXISTS ( SELECT [cSKU] FROM [PER].[reflejoBaseVenta]
WHERE [cSKU] = B.[cSKU] )

UPDATE [PER].[reflejoBaseVenta]
SET estado = IIF(B.cSKU IS NULL,0,IIF(estado=0,1,estado)  )
FROM [PER].[reflejoBaseVenta] A
LEFT JOIN [PER].[VW_ECommerceBaseVenta] B ON A.cSKU = B.cSKU


UPDATE [PER].[reflejoBaseVenta]
SET estado = 1
FROM [PER].[reflejoBaseVenta] A
JOIN [PER].[articuloImagen] B ON A.cSKU = B.sku 
WHERE A.estado = 2
end

GO
/****** Object:  StoredProcedure [PER].[SP_actualizarPrecio]    Script Date: 7/10/2017 2:57:34 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [PER].[SP_actualizarPrecio]
(
 @sku VARCHAR(50),
 @NewPrecio money
)
AS BEGIN 
		UPDATE [PER].[reflejoBaseVenta]
		SET nPrecioVenta = @NewPrecio
		WHERE [cSKU] = @sku

		UPDATE DBNegocio.dbo.INV_ArticuloStock SET nPrecioVenta =@NewPrecio  WHERE cSKU=@sku

END
GO
