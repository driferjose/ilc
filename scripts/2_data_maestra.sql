USE [DBECommerce]
GO
INSERT [PER].[admin_token] ([token]) VALUES (N'b0XAym5nSermOOuXsPwCg34nD7Utk5twlizbzyvp2UyobzS96XMq')
SET IDENTITY_INSERT [PER].[admin_usuario] ON 

INSERT [PER].[admin_usuario] ([idAdminUsuario], [usuario], [password]) VALUES (1, N'admin', N'21232f297a57a5a743894a0e4a801fc3')
SET IDENTITY_INSERT [PER].[admin_usuario] OFF
INSERT [PER].[estado] ([idEstado], [descripcion]) VALUES (0, N'inactivo')
INSERT [PER].[estado] ([idEstado], [descripcion]) VALUES (1, N'en venta')
INSERT [PER].[estado] ([idEstado], [descripcion]) VALUES (2, N'sin imagen')
INSERT [PER].[estado] ([idEstado], [descripcion]) VALUES (3, N'desahabilitado por administrador')
SET IDENTITY_INSERT [PER].[indicador] ON 

INSERT [PER].[indicador] ([idIndicador], [descripcion]) VALUES (1, N'Productos más vistos')
INSERT [PER].[indicador] ([idIndicador], [descripcion]) VALUES (2, N'Usuarios registrados')
INSERT [PER].[indicador] ([idIndicador], [descripcion]) VALUES (3, N'Productos Separados')
SET IDENTITY_INSERT [PER].[indicador] OFF
SET IDENTITY_INSERT [PER].[rangoPrecios] ON 

INSERT [PER].[rangoPrecios] ([nIdRango], [cNombreRango], [cValorMinimo], [cValorMaximo], [cEstado]) VALUES (1, N'Hasta S/500', 0, 500, 1)
INSERT [PER].[rangoPrecios] ([nIdRango], [cNombreRango], [cValorMinimo], [cValorMaximo], [cEstado]) VALUES (2, N'S/500 a S/1000', 501, 1000, 1)
INSERT [PER].[rangoPrecios] ([nIdRango], [cNombreRango], [cValorMinimo], [cValorMaximo], [cEstado]) VALUES (3, N'Más de S/ 1000', 1001, 10000, 1)
SET IDENTITY_INSERT [PER].[rangoPrecios] OFF
INSERT [PER].[reflejoFamilia] ([nCodFamilia], [iconoUrl]) VALUES (31, N'assets/img/familias/31.png')
INSERT [PER].[reflejoFamilia] ([nCodFamilia], [iconoUrl]) VALUES (20, N'assets/img/familias/20.png')
INSERT [PER].[reflejoFamilia] ([nCodFamilia], [iconoUrl]) VALUES (30, N'assets/img/familias/30.png')
INSERT [PER].[reflejoFamilia] ([nCodFamilia], [iconoUrl]) VALUES (14, N'assets/img/familias/14.png')
INSERT [PER].[reflejoFamilia] ([nCodFamilia], [iconoUrl]) VALUES (6, N'assets/img/familias/6.png')
INSERT [PER].[reflejoFamilia] ([nCodFamilia], [iconoUrl]) VALUES (27, N'assets/img/familias/27.png')
INSERT [PER].[reflejoFamilia] ([nCodFamilia], [iconoUrl]) VALUES (7, N'assets/img/familias/7.png')
INSERT [PER].[reflejoFamilia] ([nCodFamilia], [iconoUrl]) VALUES (12, N'assets/img/familias/12.png')
INSERT [PER].[reflejoFamilia] ([nCodFamilia], [iconoUrl]) VALUES (13, N'assets/img/familias/13.png')
INSERT [PER].[reflejoFamilia] ([nCodFamilia], [iconoUrl]) VALUES (19, N'assets/img/familias/19.png')
INSERT [PER].[reflejoFamilia] ([nCodFamilia], [iconoUrl]) VALUES (10, N'assets/img/familias/10.png')
INSERT [PER].[reflejoFamilia] ([nCodFamilia], [iconoUrl]) VALUES (36, N'assets/img/familias/36.png')
INSERT [PER].[reflejoFamilia] ([nCodFamilia], [iconoUrl]) VALUES (34, N'assets/img/familias/34.png')
INSERT [PER].[reflejoFamilia] ([nCodFamilia], [iconoUrl]) VALUES (15, N'assets/img/familias/15.png')
INSERT [PER].[reflejoFamilia] ([nCodFamilia], [iconoUrl]) VALUES (26, N'assets/img/familias/26.png')
INSERT [PER].[reflejoFamilia] ([nCodFamilia], [iconoUrl]) VALUES (5, N'assets/img/familias/5.png')
INSERT [PER].[reflejoFamilia] ([nCodFamilia], [iconoUrl]) VALUES (2, N'assets/img/familias/2.png')
INSERT [PER].[reflejoFamilia] ([nCodFamilia], [iconoUrl]) VALUES (35, N'assets/img/familias/35.png')
INSERT [PER].[reflejoFamilia] ([nCodFamilia], [iconoUrl]) VALUES (29, N'assets/img/familias/29.png')
INSERT [PER].[reflejoFamilia] ([nCodFamilia], [iconoUrl]) VALUES (18, N'assets/img/familias/18.png')
INSERT [PER].[reflejoFamilia] ([nCodFamilia], [iconoUrl]) VALUES (22, N'assets/img/familias/22.png')
INSERT [PER].[reflejoFamilia] ([nCodFamilia], [iconoUrl]) VALUES (9, N'assets/img/familias/9.png')
INSERT [PER].[reflejoFamilia] ([nCodFamilia], [iconoUrl]) VALUES (1, N'assets/img/familias/1.png')
INSERT [PER].[reflejoFamilia] ([nCodFamilia], [iconoUrl]) VALUES (28, N'assets/img/familias/28.png')
INSERT [PER].[reflejoFamilia] ([nCodFamilia], [iconoUrl]) VALUES (25, N'assets/img/familias/25.png')
INSERT [PER].[reflejoFamilia] ([nCodFamilia], [iconoUrl]) VALUES (8, N'assets/img/familias/8.png')
INSERT [PER].[reflejoFamilia] ([nCodFamilia], [iconoUrl]) VALUES (17, N'assets/img/familias/17.png')
INSERT [PER].[reflejoFamilia] ([nCodFamilia], [iconoUrl]) VALUES (23, N'assets/img/familias/23.png')
INSERT [PER].[reflejoFamilia] ([nCodFamilia], [iconoUrl]) VALUES (32, N'assets/img/familias/32.png')
INSERT [PER].[reflejoFamilia] ([nCodFamilia], [iconoUrl]) VALUES (11, N'assets/img/familias/11.png')
SET IDENTITY_INSERT [PER].[servicioSMS] ON 

INSERT [PER].[servicioSMS] ([id], [usuario], [clave], [url]) VALUES (1, N'wscruz', N'tYFsatT9', N'http://efitec.adminbulk.com/rest/ws/bulkSms')
SET IDENTITY_INSERT [PER].[servicioSMS] OFF
SET IDENTITY_INSERT [PER].[tiemposReserva] ON 

INSERT [PER].[tiemposReserva] ([idTiempoReserva], [ubicacion], [tiempoReserva], [dias]) VALUES (1, N'LIMA', 72, 3)
INSERT [PER].[tiemposReserva] ([idTiempoReserva], [ubicacion], [tiempoReserva], [dias]) VALUES (3, N'ANCASH', 48, 2)
INSERT [PER].[tiemposReserva] ([idTiempoReserva], [ubicacion], [tiempoReserva], [dias]) VALUES (4, N'AREQUIPA', 48, 2)
INSERT [PER].[tiemposReserva] ([idTiempoReserva], [ubicacion], [tiempoReserva], [dias]) VALUES (5, N'CALLAO', 48, 2)
INSERT [PER].[tiemposReserva] ([idTiempoReserva], [ubicacion], [tiempoReserva], [dias]) VALUES (6, N'CUSCO', 48, 2)
INSERT [PER].[tiemposReserva] ([idTiempoReserva], [ubicacion], [tiempoReserva], [dias]) VALUES (7, N'ICA', 48, 2)
INSERT [PER].[tiemposReserva] ([idTiempoReserva], [ubicacion], [tiempoReserva], [dias]) VALUES (8, N'JUNIN', 48, 2)
INSERT [PER].[tiemposReserva] ([idTiempoReserva], [ubicacion], [tiempoReserva], [dias]) VALUES (9, N'LA LIBERTAD', 48, 2)
INSERT [PER].[tiemposReserva] ([idTiempoReserva], [ubicacion], [tiempoReserva], [dias]) VALUES (10, N'LAMBAYEQUE', 48, 2)
INSERT [PER].[tiemposReserva] ([idTiempoReserva], [ubicacion], [tiempoReserva], [dias]) VALUES (11, N'LORETO', 48, 2)
INSERT [PER].[tiemposReserva] ([idTiempoReserva], [ubicacion], [tiempoReserva], [dias]) VALUES (12, N'PIURA', 48, 2)
INSERT [PER].[tiemposReserva] ([idTiempoReserva], [ubicacion], [tiempoReserva], [dias]) VALUES (13, N'PUNO', 48, 2)
INSERT [PER].[tiemposReserva] ([idTiempoReserva], [ubicacion], [tiempoReserva], [dias]) VALUES (14, N'SAN MARTIN', 48, 2)
INSERT [PER].[tiemposReserva] ([idTiempoReserva], [ubicacion], [tiempoReserva], [dias]) VALUES (15, N'UCAYALI', 48, 2)
SET IDENTITY_INSERT [PER].[tiemposReserva] OFF
